#!/usr/bin/php

<?php

/*
    This file is part of MetalMech.
    Copyright (C) 2005  Dzmitry A. Haiduchonak <boom@metalmech.com>

    MetalMech is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    MetalMech is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MetalMech; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

//auto_logout

//start
// # php -q auto_logout_1min.php &


define("DEBUG", true);

//change path to your htdocs dir
if (FALSE != DEBUG) {
    define("PATH", "/var/www/demo.metalmech.com/htdocs/");
    define("XMLDATA", "/var/www/demo.metalmech.com/htdocs/xml_data/");
    
} else {
    define("PATH", "/vhosts/boom/mm/demo/public/");
    define("XMLDATA", "/vhosts/boom/mm/demo/public/xml_data/");
}


require_once(PATH."/includes/functions.inc");

while(1) {
    auto_logout_users();
    sleep(60);
}
?>