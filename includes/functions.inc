<?php

//includes/functions.inc

/*
 * Created on 9/Set/2005
 *
 * Created by: Ricardo Martins
 */

function open_xml ($path_to_file, $readonly=false) {

    global $fp;

    $x=0;
  
    for($handler = fopen($path_to_file,"r");$handler === false;){
        
        if (FALSE != DEBUG) {

            $x++;
        

            if ($x>10) {
                print "Error unlocking ".$path_to_file;
                die();
            
            }
        }
        sleep(1);
    }

    if (flock($handler, LOCK_EX)) {

        unset($xml_text);
        while (!feof($handler)) {
            $xml_text .= fread($handler, 8192);
        }
        
        $xml = new DOMDocument();
        $ret = @$xml->loadXML($xml_text);

        if (!$ret) {
            echo "Error while parsing the document \n".$path_to_file;
            print $xml_text;
            fclose($handler);
            exit;
        }

        if ($readonly) {
            fclose($handler);
        } else {
            /*Creating or adding to the global variable fp an array with the filepath and
            * the file handler.
            */

            if(!is_array($fp)){
                $fp = array(array("file_path" => $path_to_file,"handler" => $handler));
            }
            else{
                array_push($fp,array("file_path" => $path_to_file,"handler" => $handler));
            }
        }
    }
    return $xml;
}

function save_xml ($path_to_file, $xml, $is_new=false) {

    global $fp;
    $xmlfile = $xml->saveXML();

    $status = false;
    $found=false;

    if ($is_new === false) {

        if (is_array($fp)) {
        
            foreach($fp as $key => $value) {
        
                if ($value['file_path'] == $path_to_file) {
                    $found = true;
                    fclose($value['handler']);
                    array_splice($fp,$key,1);

                    $x=0;
                    for($handler = fopen($path_to_file,"w");$handler === false;){
                        if (FALSE != DEBUG) {

                            $x++;

                            if ($x>10) {
                                print "Error locking on writing ".$path_to_file;
                                die();

                            }
                        }
                        sleep(1);
                    }
            
                    if (flock($handler, LOCK_EX)) {
                        fwrite($handler,$xmlfile);
                        fclose($handler);
                        $status = true;
                    }
                    break;
                }
            }
        }

        if ($found === false) {
            print "Error saving ".$path_to_file;
            die();
        }
    } else {
        
        $x=0;
        for($handler = fopen($path_to_file,"w");$handler === false;){

            if (FALSE != DEBUG) {
                $x++;

            

                if ($x>10) {
                    print "Error locking on writing ".$path_to_file;
                    die();

                }
            }
            sleep(1);
        }
        
                        
        if (flock($handler, LOCK_EX)) {
            fwrite($handler, $xmlfile);
            fclose($handler);
            $status = true;
        }

    }
    return $status;
}

function unlock_xml ($path_to_file) {

    global $fp;

    if (is_array($fp)) {
        
        foreach($fp as $key => $value) {

            if ($value['file_path'] == $path_to_file) {

                fclose($value['handler']);
                array_splice($fp,$key,1);
            }
        }
    }
}

function list_xml_handlers () {

    global $fp;

    if (is_array($fp)) {
        
        foreach($fp as $key => $value) {

            print $value['file_path']." ".$value['handler'];

        }
    }
}

//service values "+X, -X"
function serv_val($val="", $retval="") {
    $val=strval($val);
    $div=substr($val,0,1);
    $dig=floatval(substr($val,1));

    switch ($div){
        case "+":
            $retval+=$dig;
        break;

        case "-":
            $retval-=$dig;
        break;

        case "/":
            $retval/=$dig;
        break;

        case "*":
            $retval*=$dig;
        break;

        default:
            $retval=floatval($val);
        break;

    }
    return $retval;
}
//-------------------------------
function get_ip() {
if($ip = getenv("HTTP_CLIENT_IP")) return $ip;

   if ($ip = getenv("HTTP_X_FORWARDED_FOR"))
   {
      if ($ip == '' || $ip == "unknown")
      {
          $ip = getenv("REMOTE_ADDR");
      }
      return $ip;
   }

   if ($ip = getenv("REMOTE_ADDR")) return $ip;
}


function pp($a) {
    return addslashes(htmlentities($a,ENT_QUOTES,"utf-8"));
}

function np($a) {
    return html_entity_decode(stripslashes($a), ENT_QUOTES,"utf-8");
}

function co($a) {
    return htmlentities($a, ENT_QUOTES, "utf-8");
}

function de($a) {
    return html_entity_decode($a, ENT_QUOTES, "utf-8");
}

function randomString($len) {
   //srand(date("s"));
   $possible="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
   $str="";
   while(strlen($str)<$len) {
      $str.=substr($possible,(rand()%(strlen($possible))),1);
   }
   return($str);
}

function randomStringHex($len) {
   //srand(date("s"));
   $possible="ABCDEF1234567890";
   $str="";
   while(strlen($str)<$len) {
      $str.=substr($possible,(rand()%(strlen($possible))),1);
   }
   return($str);
}


function GetVar($var) {
    
    global $_SESSION;
    global $_GET;
    unset($ret);
    
    if (!empty($_SESSION[$var])) {
        $ret=$_SESSION[$var];
    }

    if (!empty($_GET[$var])) {
        $ret=$_GET[$var];
        $_SESSION[$var]=$ret;

    }

    if (!empty($_POST[$var])) {
        $ret=$_POST[$var];
        $_SESSION[$var]=$ret;

    }
    
    return $ret;
}

function Warn($text, $line) {

    global $Err;
    $Err=array();
    if (FALSE != DEBUG) {
    //    print __FILE__." [".$line."] ".$text;
    }
    $Err[]=__FILE__." [".$line."] ".$text;
}

function Error($text, $line) {
    if (FALSE != DEBUG) {
        print __FILE__." [".$line."] ".$text;
    }
    die();
}

function show_profiling() {
    global $timer;
    global $smarty;

    if (FALSE != DEBUG) {
        $timer->stop();
        $profiling = $timer->getProfiling();
        $smarty->assign('profiling', $profiling);
    }
}

//check user login
//if true return 1
function chk_login() {

    $status = 0;
    $login = filter_login($_SESSION["login"]);
    $_SESSION["insystem"] = 0;
    unset($_SESSION["insystem"]);

    if (is_file(XMLDATA."pilot/pilot_".$login.".xml")) {
        

        $verifycode = $_SESSION["verifycode"];
        
        $my_ip=get_ip();
    
        //check login, cw, ip match
        $pilot = open_xml(XMLDATA."pilot/pilot_".$login.".xml");

        $xpath = new domXPath($pilot);
    
        $pilot_q = $xpath->query("/pilot");
    
        if ($pilot_q->length>0) {
    
            $login2 = de($pilot_q->item(0)->getAttribute("login"));
            
            if ($login2 == $login) {
                $pilot_login = $xpath->query("/pilot/login");
    
                if ($pilot_login->length>0) {
                    $stat=$pilot_login->item(0)->getAttribute("status");
    
                    if ($stat == "insystem") {
                        
                        $sid_tag = $pilot_login->item(0)->getElementsByTagName("sid");
                        $sid = de($sid_tag->item(0)->getAttribute("val"));
    
                        $ip_tag = $pilot_login->item(0)->getElementsByTagName("ip");
                        $ip = de($ip_tag->item(0)->getAttribute("val"));
    
                        $cw_tag = $pilot_login->item(0)->getElementsByTagName("cw");
                        $cw = de($cw_tag->item(0)->getAttribute("val"));
    
                        if ((!empty($verifycode)) && ($cw == $verifycode) && ($ip == $my_ip) && ($sid == session_id())) {
                            
                            $pilot_login->item(0)->setAttribute("date", time());
                            save_xml(XMLDATA."pilot/pilot_".$login.".xml", $pilot);
                            $status = 1;
                            $_SESSION["insystem"] = 1;
                        }
                    }
                }
            }
        }
    }
    
    return $status;
}

function go_to_login($returl="") {

    global $loginurl;
    header("Location: ".$loginurl."&returl=".$returl);
}

function get_online_users() {

    $users=array();
    $dir=getFiles(XMLDATA."pilot");
    //print_r($dir);
    while (list($key, $val) = each($dir)) {
        //check if file is xml

        unset($pilot);
        unset($ret);

        $pilot = open_xml($val, true);

        $xpath = new domXPath($pilot);
    
        $pilot_q = $xpath->query("/pilot");
    
        if ($pilot_q->length>0) {
            $login2 = de($pilot_q->item(0)->getAttribute("login"));
            $name = de($pilot_q->item(0)->getAttribute("name"));
            
            $pilot_login = $xpath->query("/pilot/login");

            if ($pilot_login->length>0) {
                $status=$pilot_login->item(0)->getAttribute("status");
                $battlestatus=$pilot_login->item(0)->getAttribute("battlestatus");

                if ($status == "insystem") {
                    $ip_tag = $pilot_login->item(0)->getElementsByTagName("ip");
                    $ip = de($ip_tag->item(0)->getAttribute("val"));
                    $users[$login2]=array("login"=>$login2, "nick"=>$name, "ip"=>$ip, "status" => $status, "battlestatus" => $battlestatus);
                }
            }
        }        
    }
    return $users;
}

function auto_logout_users() {

    $dir=getFiles(XMLDATA."pilot");
    //print_r($dir);
    while (list($key, $val) = each($dir)) {
        //check if file is xml

        unset($pilot);
        unset($ret);

        $pilot = open_xml($val);
        
        $xpath = new domXPath($pilot);
    
        $pilot_q = $xpath->query("/pilot");
        unset($login2);
                
        if ($pilot_q->length>0) {
            $login2 = filter_login(de($pilot_q->item(0)->getAttribute("login")));
            $name = de($pilot_q->item(0)->getAttribute("name"));
            
            $pilot_login = $xpath->query("/pilot/login");

            if ($pilot_login->length>0) {
                $status=$pilot_login->item(0)->getAttribute("status");
                $date=$pilot_login->item(0)->getAttribute("date");

                if ($status == "insystem") {
                
                    if ((time()-$date) > 10*60) {
                    
                        $sid_tag = $pilot_login->item(0)->getElementsByTagName("sid");
                        $sid_tag->item(0)->setAttribute("val", co($sid));

                        $ip_tag = $pilot_login->item(0)->getElementsByTagName("ip");
                        $ip_tag->item(0)->setAttribute("val", co($ip));

                        $cw_tag = $pilot_login->item(0)->getElementsByTagName("cw");
                        $cw_tag->item(0)->setAttribute("val", co($cw));
                        $pilot_login->item(0)->setAttribute("status", "none");
                        $pilot_login->item(0)->setAttribute("date", time());

                        $pilot_l = $xpath->query("/pilot/status/battle");
                        save_xml(XMLDATA."pilot/pilot_".$login2.".xml", $pilot);
                        
                        if ($pilot_l->length>0) {
                            $battlestatus=$pilot_l->item(0)->getAttribute("status");

                            if ($battlestatus=="wait") {
//kill sign for battle from battlelist
                                cancel_sign_for_battle($login2);
                            }
           
                        }
                        
                        print date("m.d.y H:i:s", time())."  Pilot ".$login2." terminated.\n";
                    }
    // check if pilot ib battle or newbattle and cancel sign                
                }
            }
        }        
    }
    unset($dir);
}

function getFiles($directory) {
   // Try to open the directory
   if($dir = opendir($directory)) {
       // Create an array for all files found
       $tmp = Array();

       // Add the files
       while($file = readdir($dir)) {
           // Make sure the file exists
           if($file != "." && $file != ".." && $file[0] != '.') {
               // If it's a directiry, list all files within it
               if(is_dir($directory . "/" . $file)) {
                   $tmp2 = getFiles($directory . "/" . $file);
                   if(is_array($tmp2)) {
                       $tmp = array_merge($tmp, $tmp2);
                   }
               } else {
                   array_push($tmp, $directory . "/" . $file);
               }
           }
       }

       // Finish off the function
       closedir($dir);
       return $tmp;
   }
}


function pregtrim($str) {
   return preg_replace("/[^\x20-\xFF]/","",@strval($str));
}

function checkmail($mail) {
   // ����� ����� ������� � ������� �������
   $mail=trim(pregtrim($mail)); // ������� pregtrim() �������� ���� � �������
   // ���� ����� - �����
   if (strlen($mail)==0) return 1;
   if (!preg_match("/^[a-z0-9_-]{1,20}@(([a-z0-9-]+\.)+(com|net|org|mil|".
   "edu|gov|arpa|info|biz|inc|name|[a-z]{2})|[0-9]{1,3}\.[0-9]{1,3}\.[0-".
   "9]{1,3}\.[0-9]{1,3})$/is",$mail))
   return -1;
   return $mail;
}

function filter_login($login) {

    $login=trim(pregtrim(de($login)));
    return  strtolower(preg_replace("/[^a-zA-Z0-9]/","",$login));

}


function cancel_sign_for_battle($login) {
        //cancel sign for battle

        //check if can cancel

        //remove pilot from battlelist

        //update pilot status

    $login = filter_login($login);

    $battlestatus = get_pilot_battlestatus($login);

    if ($battlestatus != "battlebegin") {

        $error=0;

        $battlelist = open_xml(XMLDATA."battle/battlelist.xml");

        $xpath2 = new domXPath($battlelist);

        $owner=0;

        $battle_pilot = $xpath2->query("//pilot[@login='".$login."']");
    //check if if first chilf or not
        if ($battle_pilot->length>0) {
            $pilot_list=$battle_pilot->item(0)->parentNode->getElementsByTagName('pilot');

            $k=0;
            $plist=array();

            foreach ($pilot_list as $pilot_list_tags) {
                $pilot_login=$pilot_list_tags->getAttribute("login");
                if (($k == 0) && ($pilot_login == $login)) {
                    $owner=1;
                }
                $plist[]=$pilot_login;
                $k++;
            }


            $pplist=array();

            if ($owner) {

                $parent=$battle_pilot->item(0)->parentNode;
                $parent->parentNode->removeChild($parent);
                $pplist=$plist;
            } else {
                $pplist[]=$login;
                $battle_pilot->item(0)->parentNode->removeChild($battle_pilot->item(0));
            }


            while(list($kk, $plogin)=each($pplist)) {
                unset($xml_out);
                unset($xpath);
                
                if (is_file(XMLDATA."pilot/pilot_".$plogin.".xml")) {
                    $xml_out = open_xml(XMLDATA."pilot/pilot_".$plogin.".xml");

                    $xpath = new domXPath($xml_out);
                    unset($pilot);
                    $pilot = $xpath->query("/pilot");
                    unset($login2);
                    $login2=de($pilot->item(0)->getAttribute("login"));
                    $nick=de($pilot->item(0)->getAttribute("name"));

                    $battle = $xpath->query("/pilot/status/battle");
                    //$battlestatus=$battle->item(0)->getAttribute("status");
                    $battle->item(0)->setAttribute("status", "free");
                    $battle->item(0)->setAttribute("date", time());
                    save_xml(XMLDATA."pilot/pilot_".$plogin.".xml", $xml_out);
                }
            }

        } else {
            $error=1;
        }

        if (!$error) {
            save_xml(XMLDATA."battle/battlelist.xml", $battlelist);

        } else {
            unlock_xml(XMLDATA."battle/battlelist.xml");

        }
    }
}


function get_pilot_battlestatus($login) {
/*

1st- wait - waitforenemy
2nd - none

1st - wait        -waitforbattle
2nd - accepted - waitforaccept
= wait for accept
----------
1st - accepted
2nd - acccepted
= battle begin


*/

    $battlestatus = "free";

    if (!empty($login)) {

        if (is_file(XMLDATA."battle/battlelist.xml")) {
        
            $battlelist = open_xml(XMLDATA."battle/battlelist.xml", true);

            $xpath = new domXPath($battlelist);
            $battle_q = $xpath->query("/battlelist/battle//pilot[@login='".$login."']");

            if ($battle_q->length>0) {


                $battle = $battle_q->item(0)->parentNode;

                $pilot_tags = $battle->getElementsByTagName("pilot");

                $is_enemy=0;


                foreach ($pilot_tags as $pilot) {

                    $pilot_login = de($pilot->getAttribute("login"));
                    $pilot_nick = de($pilot->getAttribute("name"));

                    $pilot_date = $pilot->getAttribute("date");
                    $pilot_bidamount = de($pilot->getAttribute("bidamount"));

                    if ($pilot_login == $login) {
                        $is_enemy=0;
                    } else {
                        $is_enemy=1;
                    }

                    if (!$is_enemy) {
                        $first_status = $pilot->getAttribute("status");
                    } else {
                        $second_status = $pilot->getAttribute("status");
                    }


                }

                if (($first_status == "accepted") && ($second_status == "accepted")) {
                    $battlestatus="battlebegin";
                } else  if (($first_status == "wait") && ($second_status == "accepted")) {
                    $battlestatus = "waitforaccept";
                } else  if (($first_status == "accepted") && ($second_status == "wait")) {
                    $battlestatus = "waitforbattle";
                }   else if (($first_status == "wait") && (empty($second_status))){
                    $battlestatus = "waitforenemy";
                }
            }
        }
    }

    return $battlestatus;

}

?>