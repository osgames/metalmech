<?php
//dev.php

require_once("includes/constants.inc");

?>
<html>

<head>
  <title>MetalMech Dev Section</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link title="css"  href="/styles.css" type="text/css" rel="stylesheet"/>
<body leftmargin="0" topmargin="0">
<table border="0" cellspacing="1" width="100%" cellpadding="0" bgcolor="#F5FFF5">
<tr>
<td style="font-family : Verdana,Arial; font-size : 11px;" valign="top" align="center" bgcolor="#FFCC00">
<a target="_blank" href="http://dev.metalmech.com/index.php/Manual">Game Manual</a>
</td>
<td style="font-family : Verdana,Arial; font-size : 11px;" valign="top" align="center" bgcolor="#FF3333">
<a target="_blank" href="http://sourceforge.net/tracker/?func=add&group_id=123504&atid=696933">Report a bug</a>
</td>
<td style="font-family : Verdana,Arial; font-size : 11px;" valign="top" align="center" bgcolor="#66FF33">
<a target="_blank" href="http://forum.metalmech.com">Support Forum</a>
</td>
<td style="font-family : Verdana,Arial; font-size : 11px;" valign="top" align="center" bgcolor="#CC99CC">
<a target="_blank" href="http://dev.metalmech.com">Game Development</a>
</td>
<td style="font-family : Verdana,Arial; font-size : 11px;" valign="top" align="center" bgcolor="#FF99CC">
<a target="_blank" href="http://sourceforge.net/projects/metalmech">Project page on SourceForge</a>
</td>

<td style="font-family : Verdana,Arial; font-size : 11px;" valign="top" align="center" rowspan="2"><a href="http://sourceforge.net"><img src="http://sourceforge.net/sflogo.php?group_id=123504&type=1" width="88" height="31" border="0" alt="SourceForge.net Logo" /></a></td>
<td style="font-family : Verdana,Arial; font-size : 11px;" valign="top" align="center" rowspan="2"><!-- Akavita counter code --><a target=_top
href="http://adlik.akavita.com/bin/link?id=13778">
<script language=javascript><!--
d=document;w=window;n=navigator;d.cookie="cc=1";
r=''+escape(d.referrer);js=10;c=(d.cookie)?1:0;j=0;
x=Math.random();u=''+escape(w.location.href);lt=0;
h=history.length;t=new Date;f=(self!=top)?1:0;cd=0;
tz=t.getTimezoneOffset();cpu=n.cpuClass;ww=wh=ss=0;
//--></script><script language="javascript1.1"><!--
js=11;j=(n.javaEnabled()?1:0);
//--></script><script language="javascript1.2"><!--
js=12;lt=1;s=screen;ss=s.width;
cd=(s.colorDepth?s.colorDepth:s.pixelDepth);
//--></script><script language="javascript1.3"><!--
js=13;wh=w.innerHeight;ww=w.innerWidth;
wh=(wh?wh:d.documentElement.offsetHeight);
ww=(ww?ww:d.documentElement.offsetWidth);
//--></script><script language=javascript><!--
q='lik?id=13778&d='+u+'&r='+r+'&h='+h+'&f='+f;
q+='&c='+c+'&tz='+tz+'&cpu='+cpu+'&js='+js+'&wh='+wh;
q+='&ww='+ww+'&ss='+ss+'&cd='+cd+'&j='+j+'&x='+x;
d.write('<img src="http://adlik.akavita.com/bin/'+
q+'" alt="Akavita" '+
'border=0 width=88 height=31>');
if(lt){d.write('<'+'!-- ');}//--></script><noscript>

<img src="http://adlik.akavita.com/bin/lik?id=13778"
border=0 height=31 width=88 alt="Akavita">
</noscript><script language="JavaScript"><!--
if(lt){d.write('--'+'>');}//--></script></a>
<!-- Akavita counter code --></td>

</tr>
<tr>
<td colspan="5">
<table border="0" cellspacing="1"  cellpadding="1">
<td style="font-family : Verdana,Arial; font-size : 11px;" bgcolor="#99CCCC">&nbsp;&nbsp;&nbsp;Contact e-mail:&nbsp;<a href="mailto: boom@metalmech.com">boom@metalmech.com</a>&nbsp;&nbsp;&nbsp;</td><td style="font-family : Verdana,Arial; font-size : 11px;" bgcolor="#9999CC">&nbsp;&nbsp;&nbsp;IRC:&nbsp;#metalmech at <a href="http://www.freenode.net">freenode.net</a>&nbsp;&nbsp;&nbsp;</td>
<td  style="font-family : Verdana,Arial; font-size : 11px;" bgcolor="#99CCCC">&nbsp;&nbsp;&nbsp;ICQ UIN:&nbsp;<u>21824586</u>&nbsp;&nbsp;&nbsp;</td>
</tr></table>
</td>
</tr>
<tr><td style="font-family : Verdana,Arial; font-size : 11px; color: #000000;"  bgcolor="#F5FFF5" valign="top" align="center"  colspan="6">

<?php


if (defined("VER")) {
    print "<b>Version:</b> ".(VER);

    if (defined("VERSTATUS")) {
        print "-".VERSTATUS;
    }
}

print "&nbsp;&nbsp;&nbsp;<b>Load Avg:</b> ";

$loadavg=loadavg();
print $loadavg[0]." ".$loadavg[1]." ".$loadavg[2];
print "&nbsp;&nbsp;&nbsp;<b>Uptime:</b> ".uptime();
?>
</td>
</tr>

</table>

</body>
</html>
<?php
function uptime () {
    
    $fd = fopen('/proc/uptime', 'r');
    $ar_buf = split(' ', fgets($fd, 4096));
    fclose($fd);

    $sys_ticks = trim($ar_buf[0]);

    $min = $sys_ticks / 60;
    $hours = $min / 60;
    $days = floor($hours / 24);
    $hours = floor($hours - ($days * 24));
    $min = floor($min - ($days * 60 * 24) - ($hours * 60));

    if ($days != 0) {
      $result = $days." days ";
    } 

    $result .= $hours;
    $result .= ":".sprintf("%02d",$min);

    return $result;
  }
  
  function loadavg () {
    if ($fd = fopen('/proc/loadavg', 'r')) {
      $results = split(' ', fgets($fd, 4096));
      fclose($fd);
    } else {
      $results = array('N.A.', 'N.A.', 'N.A.');
    } 
    return $results;
  } 
?>