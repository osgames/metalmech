<?php

/*
    This file is part of MetalMech.
    Copyright (C) 2005  Dzmitry A. Haiduchonak <boom@metalmech.com>

    MetalMech is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    MetalMech is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MetalMech; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

//newbattle.php

define("DEBUG", true);

if (FALSE != DEBUG) {

    require_once 'Benchmark/Timer.php';
 
    $timer = new Benchmark_Timer();
    $timer->start();
}

require 'Smarty/Smarty.class.php';

$smarty = new Smarty;

$smarty->compile_check = true;

if (FALSE != DEBUG) {
    $smarty->debugging = false;
} else {
    $smarty->debugging = false;
}


include_once("../global.inc");

$smarty->template_dir = DATA.'/templates/battle/newbattle/';
$smarty->compile_dir = DATA.'/templates_c/battle/newbattle/';
$smarty->config_dir = DATA.'/configs/';

$site=GLOBALSITE."/battle/newbattle/";

require_once(DATA."/includes/functions.inc");

$smarty->assign("site", $site);
$smarty->assign("globalsite", $globalsite);

require_once(DATA."/includes/functions.inc");

$smarty->assign("site", $site);

session_start();

//get session id
$sid=session_id();




define("SID", $sid);
Warn("SID=".SID,__LINE__);


if (chk_login()) {
    
    //get action
    $action=$_GET["action"];
    if (empty($action)) {
        $action=$_POST["action"];
    }
        
    switch($action) {
        
        
        case "sign":
        //rewrite to use xslt
//check if battlelist in xml_data if not - create it
            $login=filter_login($_SESSION["login"]);
            $enemylogin=filter_login($_GET["login"]);
            
            unset($battlestatus);

            if ($login != $enemylogin) {
           
    
                if (!is_file(XMLDATA."battle/battlelist.xml")) {
                    //create battlelist.xml
                    $battlelist = open_xml(PATH."/xml/battlelist_default.xml", true);
                    save_xml(XMLDATA."battle/battlelist.xml", $battlelist, true);
                }
    //check if user not in battle and not in battlelist - add it to battlelist
    
                if (is_file(XMLDATA."pilot/pilot_".$login.".xml")) {
                    
                    $xml_out = open_xml(XMLDATA."pilot/pilot_".$login.".xml");
            
                    $xpath = new domXPath($xml_out);

                    $pilot = $xpath->query("/pilot");
                    $login2=de($pilot->item(0)->getAttribute("login"));
                    $nick=de($pilot->item(0)->getAttribute("name"));

                    $battle = $xpath->query("/pilot/status/battle");
                    $battlestatus=$battle->item(0)->getAttribute("status");

                    if (empty($battlestatus)) {
                        $battlestatus="free";
                    }

                    if ($battlestatus == "free") {
                        unset($battlelist);
                        $battlelist = open_xml(XMLDATA."battle/battlelist.xml");
                        $xpath = new domXPath($battlelist);
                        $xpath2 = new domXPath($battlelist);

                        $battle_q = $xpath->query("/battlelist");

                        if ($battle_q->length>0) {

                // add <battle>
                            $ok=0;

                            if (empty($enemylogin)) {
                                $battle_tag = $battlelist->createElement("battle");
                                $battle_root = $battle_q->item(0)->appendChild($battle_tag);
                                $battle_root->setAttribute("status", "wait");
                                $battle_root->setAttribute("maxplayers", "1");
                                $battle_root->setAttribute("battletype", "1v1");
                                $ok=1;
                            } else {
                                //find battle with pilot $enemylogin

                                $battle_pilot = $xpath2->query("//pilot[@login='".$enemylogin."']");

                                if ($battle_pilot->length>0) {

                                    $battle_root = $battle_pilot->item(0)->parentNode;
                                    $ok=1;
                                }
                            }

                            if ($ok) {
                                $pilot_tag = $battlelist->createElement("pilot");
                                $pilot_root = $battle_root->appendChild($pilot_tag);
                                $pilot_root->setAttribute("login", $login);
                                // ! need check
                                $pilot_root->setAttribute("bidamount", co($_POST["bidamount"]));
                                $pilot_root->setAttribute("date", time());
                                $pilot_root->setAttribute("name", $nick);

                                if (!empty($enemylogin)) {
                                    $pilot_root->setAttribute("status", "accepted");
                                } else {
                                    $pilot_root->setAttribute("status", "wait");
                                }

                                $battle->item(0)->setAttribute("status", "wait");
                                $battle->item(0)->setAttribute("date", time());


                                save_xml(XMLDATA."battle/battlelist.xml", $battlelist);
                                save_xml(XMLDATA."pilot/pilot_".$login.".xml", $xml_out);

                                $msg="You signed to new battle";
                               
                               
                            } else {

                            }
                        }
                    }
                }
            }
            
            //list_xml_handlers ();
       //     unlock_xml(XMLDATA."battle/battlelist.xml");
            $bstatus = get_pilot_battlestatus($login);
            //list_xml_handlers ();
            pilot_wait_for_battle($login, $bstatus);
            //list_xml_handlers ();

        break;

        case "accept":

            $login=filter_login($_SESSION["login"]);
            $enemylogin=filter_login($_POST["login"]);

            $plist=array();
            
            $battlelist = open_xml(XMLDATA."battle/battlelist.xml");

            $xpath2 = new domXPath($battlelist);

//print $login;
            $battle_pilot = $xpath2->query("//pilot[@login='".$login."']");

            if ($battle_pilot->length>0) {
                $pilotlist = $battle_pilot->item(0)->parentNode;
                
                $pilot_root = $battle_pilot->item(0);

                //$pilot_root->setAttribute("login", $login);
                // ! need check
               // $pilot_root->setAttribute("bidamount", $_POST["bidamount"]);
                $pilot_root->setAttribute("date", time());
                //$pilot_root->setAttribute("name", $nick);
                $pilot_root->setAttribute("status", "accepted");

                $pilotlist_tags = $pilotlist->getElementsByTagName("pilot");
                
                foreach($pilotlist_tags as $pilot_tags) {
                    $login22 = de($pilot_tags->getAttribute("login"));

                    if (!empty($login22)) {
                        $plist[] = $login22;
                    }
                    
                    unset($login22);
                }
                
            }
            
            save_xml(XMLDATA."battle/battlelist.xml", $battlelist);
            
            $battlestatus = get_pilot_battlestatus($_SESSION["login"]);
            
            if ($battlestatus == "battlebegin") {
            //change status in <battle>
            //generate bid +
            //generate battle.xml
            //write refresh
            
                $battle = open_xml(PATH."/xml/battle_default.xml", true);
                                
                $xpath = new domXPath($battle);
                
                $battle_q = $xpath->query("/battle");
                
                if ($battle_q->length>0) {
                    do {
                        $bid=randomStringHex(32);
                    } while (is_file(XMLDATA."battle/battle_".$bid.".xml"));
                    
                    $battle_root=$battle_q->item(0);
                    $battle_root->setAttribute("bid", $bid);
                    $battle_root->setAttribute("type", "1v1");  
                    
                    $battle_t = $xpath->query("/battle/teams");

                    if ($battle_t->length>0) {
                        $battle_teams = $battle_t->item(0);
                    
                    //    $xpath = new domXPath($battle_root);
                        $error=0;
                    //insert mechs from pilot.xml
                        while (list($k,$alogin)=each($plist)) {
                            if (is_file(XMLDATA."pilot/pilot_".$alogin.".xml")) {
                            //create battlelist.xml
                
                                $pilot = open_xml(XMLDATA."pilot/pilot_".$alogin.".xml");
                                
    
                                $xpathp = new domXPath($pilot);
                                $xpathp2 = new domXPath($pilot);

                                $battles = $xpathp2->query("/pilot/status/battle");
                                //$battlestatus=$battle->item(0)->getAttribute("status");
                                $battles->item(0)->setAttribute("status", "inbattle");
                                $battles->item(0)->setAttribute("date", time());
                                
                                save_xml(XMLDATA."pilot/pilot_".$alogin.".xml", $pilot);
                                
                                $pilot_mech = $xpathp->query("world/mechs/mech");
                                $pilot_mech_tag = $pilot_mech->item(0);
                                
                                     //add <pilot> into <mech>
                                $pilot_t = $pilot->createElement("pilot");
                                $pilot_r = $pilot_mech_tag->appendChild($pilot_t);
                                $pilot_r->setAttribute("name", co($alogin));

                                
                                
                                
                                //insert into battle.xml
    //create <team>
                                $team_tag = $battle->createElement("team");
                                $team_root = $battle_teams->appendChild($team_tag);

                                $team_root->appendChild($battle->importNode($pilot_mech_tag, true));
    
                               

    
                                unset($xpathp);
                                unset($pilot_mech);
                                unset($pilot_mech_tag);
                            } else {
                                $error=1;
                            }
                    }
// reallocate mid,aid,did,wid
//rewrite for use xslt
                        $mech_old = $xpath->query("//mech");
                        $nmech=1;
                        if ($mech_old->length>0) {
                            
                            foreach($mech_old as $mech_tags) {
                                $mech_tags->setAttribute("mid", $nmech);
                                $nmech++;
                            }
                        }

                        $weapon_old = $xpath->query("//weapon");
                        $nweap=1;
                        if ($weapon_old->length>0) {
                            
                            foreach($weapon_old as $weapon_tags) {
                                $weapon_tags->setAttribute("wid", $nweap);
                                $nweap++;
                            }
                        }

                        
                        $armor_old = $xpath->query("//armor");
                        $narm=1;
                        if ($armor_old->length>0) {
                            
                            foreach($armor_old as $armor_tags) {
                                $armor_tags->setAttribute("aid", $narm);
                                $narm++;
                            }
                        }

                        
                        $device_old = $xpath->query("//device");
                        $ndev=1;
                        if ($device_old->length>0) {
                            
                            foreach($device_old as $device_tags) {
                                $device_tags->setAttribute("did", $ndev);
                                $ndev++;
                            }
                        }

                        $load_old = $xpath->query("//load");
                        $nload=1;
                        if ($load_old->length>0) {
                            
                            foreach($load_old as $load_tags) {
                                $load_tags->setAttribute("lid", $nload);
                                $nload++;
                            }
                        }

                        
                        //calculate weight
                    
                        $mech_old = $xpath->query("//mech");
                        
                        if ($mech_old->length>0) {
                            
                            foreach($mech_old as $mech_tags) {
                                $mid=$mech_tags->getAttribute("mid");
                                $Xweight=0;
                                unset($pweight);
                    
                                $weight_tags = $xpath->query("//mech[@mid=".$mid."]/body/*/weight");
                                
                                foreach($weight_tags as $weight)  {
                                    $pweight=$weight->getAttribute("val");
                                    $Xweight+=$pweight;
                                }
                                
                                $mech_weight = $xpath->query("//mech[@mid='".$mid."']/params/weight");
                                
                                if ($mech_weight->length>0) {
                                    $mech_weight->item(0)->setAttribute("val", $Xweight);
                                
                                } else {
                                    $mech_params = $xpath->query("//mech[@mid=".$mid."]/params");
                                    
                                    if ($mech_params->length>0) {
                                        $weight_tag = $battle->createElement("weight");
                                        $weight_tag2 = $mech_params->item(0)->appendChild($weight_tag);
                                        $weight_tag2->setAttribute("val", $Xweight);
                                
                                    }
                                }
                            }
                        }
                    
                        //calculate loadweight
                    
                        $mech_old = $xpath->query("//mech");
                        
                        if ($mech_old->length>0) {
                            
                            foreach($mech_old as $mech_tags) {
                                $mid=$mech_tags->getAttribute("mid");
                            
                                $Xloadweight=0;
                                unset($pweight);
                                unset($aweight);
                                unset($nweight);
                                
                                $weight_tags = $xpath->query("//mech[@mid=".$mid."]/body//*//*/weight");
                                
                                foreach($weight_tags as $weight)  {
                                    $aweight=$weight->getAttribute("val");
                                    $Xloadweight+=$aweight;
                                }
                    
                                $load_tags = $xpath->query("//mech[@mid=".$mid."]/body//*//weapon//load");
                                
                                foreach($load_tags as $load)  {
                                    $weight_tags = $xpath->query("weight", $load);
                                    $pweight=$weight_tags->item(0)->getAttribute("val");
                                    $amount_tags = $xpath->query("amount", $load);
                                    $nweight=$amount_tags->item(0)->getAttribute("val");
                    
                                    if (empty($nweight)) {
                                        $nweight=1;
                                    }
                    
                                    $Xloadweight+=$pweight*$nweight;
                                }
                    
                                $mech_loadweight = $xpath->query("//mech[@mid='".$mid."']/params/loadweight");
                                
                                if ($mech_loadweight->length>0) {
                                    $mech_loadweight->item(0)->setAttribute("val", $Xloadweight);
                                } else {
                                    $mech_params = $xpath->query("//mech[@mid=".$mid."]/params");
                                    
                                    if ($mech_params->length>0) {
                                        $loadweight_tag = $battle->createElement("loadweight");
                                        $loadweight_tag2 = $mech_params->item(0)->appendChild($loadweight_tag);
                                        $loadweight_tag2->setAttribute("val", $Xloadweight);
                                    }
                                }
                            }
                        }

                        
                        //update battlelist (bid, status)
                        unset($battlelist);
                        $battlelist = open_xml(XMLDATA."battle/battlelist.xml");
                                    
                        $xpath2 = new domXPath($battlelist);
            
            //print $login;
                        $battle_pilot = $xpath2->query("//pilot[@login='".$login."']");

                        if ($battle_pilot->length>0) {
                        
                            $pilotlist = $battle_pilot->item(0)->parentNode;              
                            $pilotlist->setAttribute("bid", $bid);
                            $pilotlist->setAttribute("status", "battle");
                            $pilotlist->setAttribute("date", time());
                            
                        } else {
                            $error=1;
                        }

                        if (!$error) {
                            save_xml(XMLDATA."battle/battlelist.xml", $battlelist);
                            save_xml(XMLDATA."battle/battle_".$bid.".xml", $battle, true);
                            unset($_SESSION["bid"]);
                            unset($_SESSION["turn"]);
                            unset($_SESSION["phase"]);
                            header("Location: ".$globalsite."/battle/?bid=".$bid."&begin=1");
                            //print "redirect";
                        } else {
                            print "error";
                        }
                    }
                }
            } else {
                pilot_wait_for_battle($_SESSION["login"], $battlestatus);
            }
        break;


        case "cancelsign":
            cancel_sign_for_battle($_SESSION["login"]);
            pilot_wait_for_battle($_SESSION["login"]);
        break;
        
        default:
            $login = filter_login($_SESSION["login"]);
    //check battlestatus and make redirections
            $battlestatus = get_pilot_battlestatus($login);

            if ($battlestatus == "battlebegin") {
                //get bid from battlelist.xml

                unset($battlelist);
                $battlelist = open_xml(XMLDATA."battle/battlelist.xml", true);
    
                $xpath = new domXPath($battlelist);
                $xpath2 = new domXPath($battlelist);
    
                $battle_q = $xpath->query("/battlelist");
    
                if ($battle_q->length>0) {
    
                    $battle_pilot = $xpath2->query("//pilot[@login='".$login."']");

                    if ($battle_pilot->length>0) {

                        $battle_root = $battle_pilot->item(0)->parentNode;
                        $bid=$battle_root->getAttribute("bid");
                        $status=$battle_root->getAttribute("status");

                        if ($status == "battle") {
                            unset($_SESSION["bid"]);
                            unset($_SESSION["turn"]);
                            unset($_SESSION["phase"]);
                            
                            
                            header("Location: ".$globalsite."/battle/?bid=".$bid."&begin=1");
                        }
                    }
                }

                
            } else {
                pilot_wait_for_battle($login, $battlestatus);
            }
        break;
        
    }
    

} else {
    go_to_login($_SERVER["REQUEST_URI"]);
}

function pilot_wait_for_battle($login, $battlestatus='free') {
//list pilots what wait for battle
    global $smarty;

//get battlestatus for $login
//parse battlelist.xml

    unset($users);
    //$battlestatus="free";

    if (!empty($login)) {
    
        if (is_file(XMLDATA."battle/battlelist.xml")) {

            $battlelist = open_xml(XMLDATA."battle/battlelist.xml", true);
    
            $xpath = new domXPath($battlelist);
            $battle_q = $xpath->query("/battlelist/battle");
    
            if ($battle_q->length>0) {
                $i=0;
                foreach ($battle_q as $battle) {
                    $i++;

                    $bstatus = $battle->getAttribute("status");

                    if ($bstatus == "wait") {
                        $pilot_tags = $battle->getElementsByTagName("pilot");
        
                        $is_enemy=0;
                        
                        
                        foreach ($pilot_tags as $pilot) {
                
                            $pilot_login = de($pilot->getAttribute("login"));
                            $pilot_nick = de($pilot->getAttribute("name"));
        
                            $pilot_date = $pilot->getAttribute("date");
                            $pilot_bidamount = $pilot->getAttribute("bidamount");
        
        //check if pilot logined in system
                            unset($pilot);
                            $pilot = open_xml(XMLDATA."pilot/pilot_".$pilot_login.".xml", true);
                        
                            $xpath = new domXPath($pilot);
                        
                            $pilot_q = $xpath->query("/pilot");
                        
                            $pilot_l = $xpath->query("/pilot/login");

                            if ($pilot_l->length>0) {
                                $stat=$pilot_l->item(0)->getAttribute("status");
                                $battlestat=$pilot_l->item(0)->getAttribute("battlestatus");

                                if ($stat == "insystem")  {
                                    if (!$is_enemy) {
                                        $users[$i]["nick"]=$pilot_nick;
                                        $users[$i]["login"]=$pilot_login;
                                        $users[$i]["enemynick"]="";
                                        $users[$i]["enemylogin"]="";
                                        $users[$i]["status"]="waitforbattle";
                                        $is_enemy=1;
                                        
                                    } else {
                                        $users[$i]["enemynick"]=$pilot_nick;
                                        $users[$i]["enemylogin"]=$pilot_login;
                                        $users[$i]["status"]="waitforaccept";
        
                                    }
                
                                    if (empty($users[$i]["wait"])) {
                                        $wait=ceil((time()-$pilot_date)/60);
                                        $users[$i]["wait"]=$wait;                    
                                    }
                                }
                            }        

                        }
                    }
                }
            }
        }
    }
    
    $smarty->assign("battlestatus", $battlestatus);
    $smarty->assign("users", $users);
    
    $names=get_enemy_name($login);
    //print_r($names);
    list($elogin, $enick)=each($names);
    
    $smarty->assign("elogin", $elogin);
    $smarty->assign("enick", $enick);

    $smarty->display('battlewait.tpl');
}

function get_enemy_name($login) {

    $names=array();

    
    if (!empty($login)) {
    
        if (is_file(XMLDATA."battle/battlelist.xml")) {
            
            $battlelist = open_xml(XMLDATA."battle/battlelist.xml", true);
    
            $xpath = new domXPath($battlelist);
            $battle_q = $xpath->query("/battlelist/battle//pilot[@login='".$login."']");
    
            if ($battle_q->length>0) {
    
                                    
                $battle = $battle_q->item(0)->parentNode;
                                   
                $pilot_tags = $battle->getElementsByTagName("pilot");

                $is_enemy=0;


                foreach ($pilot_tags as $pilot) {

                    $pilot_login = de($pilot->getAttribute("login"));
                    $pilot_nick = de($pilot->getAttribute("name"));

                    $pilot_date = $pilot->getAttribute("date");
                    $pilot_bidamount = de($pilot->getAttribute("bidamount"));

                    if ($pilot_login != $login) {
                        $names[$pilot_login]=$pilot_nick;
                    }


                }
            }
        }
    }
    
    return $names;
}

?>