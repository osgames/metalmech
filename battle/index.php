<?php

/*
    This file is part of MetalMech.
    Copyright (C) 2005  Dzmitry A. Haiduchonak <boom@metalmech.com>

    MetalMech is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    MetalMech is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MetalMech; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

//battle.php


    //need Smarty >=2.6.9
    //need PHP5
    //need DOM support  in PHP5


// know bugs:
// + 1. do not increment turns >2, if PHPSESSION same for both enemies
// 2. not dead on heat overrev
// 3. heat not calculated if no hit
// 4. need XML file locks
// 5. error storing damage result in b.xml (duplicating turn number)

// to do:
// +1. finish calculating results of rocket fire
// +2. write calculating results of pulse gun fire
// +3. write calculating overall params (+damage, +weight, +loadweight, +heat, +ammo usage) / battle begins
// +4. write in battle turn result information (+hits, +damages, other) in bottom frame
// +5. advanced mech info (+damage, +weapons, status)
// +6. implement many independent battlefields
// +7. end of turn after 120 sec wait, implement time info in b.xml/turns
// 8. external event handler
// +9. choose enemy form
// 10. mech management forms
// 11. in battle chat


define("DEBUG", true);

if (FALSE != DEBUG) {

    require_once 'Benchmark/Timer.php';
 
    $timer = new Benchmark_Timer();
    $timer->start();
}

require '../Smarty/Smarty.class.php';

$smarty = new Smarty;

$smarty->compile_check = true;

if (FALSE != DEBUG) {
    $smarty->debugging = false;
} else {
    $smarty->debugging = false;
}

include_once("../global.inc");

$smarty->template_dir = DATA.'/templates/battle/';
$smarty->compile_dir = DATA.'/templates_c/battle/';
$smarty->config_dir = DATA.'/configs/';

$site=GLOBALSITE."/battle/";

require_once(DATA."/includes/functions.inc");

$smarty->assign("site", $site);
$smarty->assign("globalsite", $globalsite);


//timeout of turn
define("TIMEOUT", 120);
$smarty->assign("timeout", TIMEOUT);
$smarty->assign("timeout000", TIMEOUT*1000); //timeout in ms, need for javascript
$refreshcode = "window.setTimeout(\"top.frames.location.href='".$site."index.php?refresh=1'\",".(TIMEOUT*250).")";
$battleendcode = "window.setTimeout(\"top.frames.location.href='".$site."index.php?end=1&action=results'\",".(10).")";
$battlecode="\"javascript: top.frames.uinfo.location='?action=uinfo'; top.frames.einfo.location='?action=einfo';top.frames.results.location='?action=results';\"";
//print $refreshcode;

$smarty->assign("refreshcode", $refreshcode);


//algoritm of battle:
/*
1. get two mechs and put it into battle.xml (over xslt) //not implemented, need for independent battlefields
1.1 init pilot params
1.2 if all ok - begin battle
2. get mech info and write battlefield
3. put mech action into b.xml  //rewrite into xslt
4. get results of actions     //rewrite into xslt
5. check status of battle, end of turn
5.1. go to 2
5.2. if battle ends - exit
*/


//1.

//loading xml descriptor of battle
//setcookie ("mm_cw", $pilot_cw);
session_start();

//get battle id
$bid=GetVar("bid");

if (empty($bid)) {
    print "No bid";
    //exception
    exit();
}

define("BID", $bid);
Warn("BID=".BID,__LINE__);

//get pilot name
$pilot_name=GetVar("login");
//print $pilot_name;
Warn("pilot_name=".$pilot_name,__LINE__);

//$pilot_ip=get_ip();
//$_SESSION["ip"]=$pilot_ip;
//$_SESSION["cw"]=$pilot_cw;

$sid=session_id();

define("SID", $sid);
Warn("SID=".SID,__LINE__);

//get action
if (chk_login()) {
    
    //get action
    $action=$_GET["action"];
    if (empty($action)) {
        $action=$_POST["action"];
    }
    
    if (($_SESSION["phase"]=="begin")  || ($_GET["begin"]==1)) {

        $battle = open_xml(XMLDATA."battle/battle_".BID.".xml");
                    
    
        $xpath = new domXPath($battle);
        $battle_q = $xpath->query("/battle");
        
        if ($battle_q->length>0) {
            $battle_root=$battle_q->item(0);
    
        }
    
        if ((!empty($pilot_name)) && (empty($_SESSION["phase"]))) {
    //1.1 init pilot params
    //get mech info from last turn
    //if not, get from <teams>
    
            $turns = $xpath->query("turns");
            
            if ($turns->length>0) {
                $curturn=$turns->item(0)->getAttribute("current");
            } else {
                $turns = $battle->createElement("turns");
                $turns1 = $battle_root->appendChild($turns);
                $turns1->setAttribute("current", "1");
            
            }
    
            $mech = $xpath->query("turns//turn[last()]//mech");
    
            if ($mech->length==0) {
                $mech = $xpath->query("teams//team//mech");
            }
        //current turn
    
////$_SESSION["turn"]=1;
    
            save_xml(XMLDATA."battle/battle_".BID.".xml", $battle);
        //print frames
            show_profiling();
            $smarty->display('index.tpl');
            $_SESSION["phase"]="battle";
            exit();
        }
    } else if (($_SESSION["phase"]=="battle") || ($_GET["phase"]=="battle"))  {
    
        switch($action) {
    
            case "fire":
                $wid=$_GET["wid"];
                $emid=$_GET["emid"];
                //print "emid=".$emid;
                $move=$_GET["move"];
                $firemove=$_GET["firemove"];
                $endturn=0; //0
    
                $battle = open_xml(XMLDATA."battle/battle_".BID.".xml");

                $xpath = new domXPath($battle);
                $battle_q = $xpath->query("/battle");
                
                if ($battle_q->length>0) {
                    $battle_root=$battle_q->item(0);
                }
    
                if (!empty($pilot_name)) {
    
    //get mech info from last turn
    //if not, get from <teams>
                    $turns_tag = $xpath->query("turns");
                    
                    if ($turns_tag->length>0) {
                        $curturn=$turns_tag->item(0)->getAttribute("current");
                    }
    
                    $mech = $xpath->query("turns//turn[@name=".($curturn-1)."]//mech");
                    
                    if ($mech->length==0) {
                        $mech = $xpath->query("teams//team//mech");
                    }
    
                    //------------
    
                    $num_pilot=0;
    
                    foreach ($mech as $mech_tags) {
                        $pilot = $mech_tags->getElementsByTagName("pilot");
                        $pilot_tags = $pilot->item(0);
                        $num_pilot++;
    
                        if ($pilot_tags->getAttribute("name") == $pilot_name) {
                            $mid=$mech_tags->getAttribute("mid");
                        }
                    }
    
                    //if ($mid>0) {
    
                        //open b_action.xml
                        //check if exists
                        //if not - create root element
                        //else check if changeable
                        //add action
                        //also add say to chat //not implemented
                        
                        //check end of turn
                        //if yes - move battle data to b.xml and recalculate values
                        
                        //lock file
                        
                    $turned=0;
                    $endturn=0;
                    $newfile=false;
                    
                    if (!is_file(XMLDATA."battle/battle_action_".BID.".xml")) {
//                        $xml_out = new DOMDocument();
   //                     $xml_out2 = @$xml_out->load(XMLDATA."battle/battle_action_".BID.".xml");


                        if ($timestamp<=0) {
                            $timestamp=time();
                            $newtime=time();
    
                        }

                        $xml_out = new DOMDocument();
                        $xml_out_node = $xml_out->createElement("turn");
                        $xml_out_node2 = $xml_out->appendChild($xml_out_node);
                        $xml_out_node2->setAttribute("name", $curturn);
                        $xml_out_node2->setAttribute("timestamp", $newtime);
                        $newfile=true;
                    } else {
                        $xml_out = open_xml(XMLDATA."battle/battle_action_".BID.".xml");

                        $xpath2 = new domXPath($xml_out);
                    // check timeout
                        $turn = $xpath2->query("/turn");
                        
                        if ($turn->length>0) {
                            $xml_out_node2=$turn->item(0);
                            
                            if ($xml_out_node2->getAttribute("name")==$curturn) {
                                $timestamp=$xml_out_node2->getAttribute("timestamp");
    
                                $actions = $xpath2->query("action", $turn->item(0));
                                
                                if ($actions->length>0) {
                                    
                                    foreach ($actions as $action_tags) {
    
                                        if ($action_tags->getAttribute("mid") == $mid) {
    
                                            $turned=1;
                                            
                                            if ($timestamp>0) {
                                                
                                                if  ((time()-$timestamp) > TIMEOUT) {
                                                    //end turn
                                                    $turned=0;
                                                    $endturn=1;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                $turned=1;
                            }
                        }
                    //check the end of turn
                    }
    
                    $smarty->assign("turned",$turned);
    
                    if ($turned == 0) {
    
                        $action_node2 = $xml_out->createElement("action");
                        $action_node = $xml_out_node2->appendChild($action_node2);
                        $action_node->setAttribute("mid", $mid);
    
                        $smarty->assign("action",$action);
                        $smarty->assign("wid",$wid);
    
                        if (!empty($wid)) {
                            //mech can move without fire
                            
                            while(list($key, $val)=each($wid)) {
                                $fire_node = $xml_out->createElement("fire");
                                $fire_node = $action_node->appendChild($fire_node);
                                $fire_node->setAttribute("wid", $val);
                                // enemy target id
    
                                $fire_node->setAttribute("mid", $emid);
                                $fire_node->setAttribute("amount", "1");
                                $fire_node->setAttribute("direction", $firemove);
                            }
                        }
    
                        $move_node = $xml_out->createElement("move");
                        $move_node = $action_node->appendChild($move_node);
                        $move_node->setAttribute("direction", $move);
                        $move_node->setAttribute("length", "1");
    
                        switch($move) {
    
                            case "forward":
                                $smarty->assign("move", "Move forward");
                            break;
    
                            case "backward":
                                $smarty->assign("move", "Move backward");
                            break;
    
                            case "left":
                                $smarty->assign("move", "Move left");
                            break;
    
                            case "right":
                                $smarty->assign("move", "Move right");
                            break;
    
                            default:
                                $smarty->assign("move", "Stay");
                            break;
                        }
    
                        switch($firemove) {
    
                            case "left":
                                $smarty->assign("firemove", "Left");
                            break;
    
                            case "right":
                                $smarty->assign("firemove", "Right");
                            break;
    
                            default:
                                $smarty->assign("firemove", "Center");
                            break;
                        }
    
                        $action_tags=$xml_out_node2->getElementsByTagName("action");
                        
                        if (($action_tags->length == $num_pilot) ){
                            $endturn=1;
                        }
    
                        //check for the end of turn
                        //$turns_tag->item(0)->setAttribute("current", $_SESSION["turn"]+1);
                                            //    print "saved";
                        //save_xml(PATH."/battle_action_".BID.".xml", $xml_out);
                        
                        if ($endturn) {
    
                        //move action info from b_action.xml into b.xml
                            //unlock b.xml
                            //rm -f b_action.xml

                            //check this
                            $turns_tag->item(0)->setAttribute("current", $curturn+1);
                        
                            $turns_tag->item(0)->appendChild( $battle->importNode($xml_out_node2, true));
                            save_xml(XMLDATA."battle/battle_".BID.".xml", $battle);
                            @unlink(XMLDATA."battle/battle_action_".BID.".xml");
                            //$xmltext = htmlentities($xml_out->dump_mem());
                            //$smarty->assign("xmltext",$xmltext);
    
                            //get action and wrote result
                            //print "endturn";
                            get_result();
                            ////$_SESSION["turn"]++;
                        } else {
                            //print "notend";
                            unlock_xml(XMLDATA."battle/battle_".BID.".xml");
                            save_xml(XMLDATA."battle/battle_action_".BID.".xml", $xml_out, $newfile);
                            //unlock
                            //$xmltext = htmlentities($xml_out->dump_mem());
                            //$smarty->assign("xmltext",$xmltext);
                        }
                    } else if ($turned==1) {
                        $smarty->assign("error","Turn ended. Waiting enemy turn.");
                    }
    //                  print "saved";
        //  save_xml(PATH."/battle_action_".BID.".xml", $xml_out);
    
                    show_profiling();
                    $emid=get_enemy_list($pilot_name);
                    
                    $smarty->display('fire.tpl');
                    
                }
            break;
    
            case "einfo":
                mech_info(1);
            break;
    
            case "uinfo":
    // info about your mech in left frame
                mech_info(0);
                
                
            break;
    
            case "battle":
    //battle in center frame
                    // check timeout and save battle_action.xml to battle.xml at the end of turn
    
                $smarty->assign("onload", $battlecode);

                if (is_file(XMLDATA."battle/battle_action_".BID.".xml")) {
//                 
                    $xml_out = open_xml(XMLDATA."battle/battle_action_".BID.".xml", true);
                    
                    //$xml_out = new DOMDocument();
                    //$ret =@$xml_out->load(XMLDATA."battle/battle_action_".BID.".xml");
        
                    
                    $xpath2 = new domXPath($xml_out);
                    $turn = $xpath2->query("//turn[last()]");
    
                    if ($turn->length>0) {
                        $xml_out_node=$turn->item(0);
                        $timestamp=$xml_out_node->getAttribute("timestamp");
                        //if ($xml_out_node->getAttribute("name")==$curturn) {
    
                            //print time()." : ".$timestamp;
                        if ($timestamp>0) {
    
                            if ((time()-$timestamp) > TIMEOUT) {
                                //save battle_action
                                $battle = open_xml(XMLDATA."battle/battle_".BID.".xml");
     
                                $xpath = new domXPath($battle);
                                $battle_q = $xpath->query("/battle");
    
                                if ($battle_q->length>0) {
                                    $battle_root=$battle_q->item(0);
                                }
    
                                $curturn=
                                $turns_tag = $xpath->query("turns");
                                $curturn=$turns_tag->item(0)->getAttribute("current");
                                $turns_tag->item(0)->setAttribute("current", $curturn+1);
                                $turns_tag->item(0)->appendChild( $battle->importNode($xml_out_node, true));
                                save_xml(XMLDATA."battle/battle_".BID.".xml", $battle);
                                unset($battle);
                                unset($xml_out);
                                @unlink(XMLDATA."battle/battle_action_".BID.".xml");
                                //$xmltext = htmlentities($xml_out->dump_mem());
                                //$smarty->assign("xmltext",$xmltext);
    
                                //get action and wrote result
                                //print "endturn";
                                get_result();
                                ////$_SESSION["turn"]++;
                            }
                        }
                        //}
                    }
                }
                    // -----------------------
      
                $battle = open_xml(XMLDATA."battle/battle_".BID.".xml", true);
                      
                $xpath = new domXPath($battle);
                $battle_q = $xpath->query("/battle");
                
                if ($battle_q->length>0) {
                    $battle_root=$battle_q->item(0);
                }
    
                if (!empty($pilot_name)) {
    //get mech info from last turn
    //if not, get from <teams>
                    $turns = $xpath->query("turns");
                    
                    if ($turns->length>0) {
                        $curturn=$turns->item(0)->getAttribute("current");
                    }
    
                    $mech = $xpath->query("turns//turn[@name=".($curturn-1)."]//mech");
                    
                    if ($mech->length==0) {
                        $mech = $xpath->query("teams//team//mech");
                    }
                    //------------
    
                    foreach ($mech as $mech_tags) {
                        $pilot = $mech_tags->getElementsByTagName("pilot");
                        $pilot_tags = $pilot->item(0);
    
                        if ($pilot_tags->getAttribute("name") == $pilot_name) {
                            $mid=$mech_tags->getAttribute("mid");
    
                            if ($mid>0) {
                                //for compatibility
                                //$turns = $turns_tag;
    
        //check player turns
                                $turned=0;
                                
                                if (is_file(XMLDATA."battle/battle_action_".BID.".xml")) {
                                    $xml_out = open_xml(XMLDATA."battle/battle_action_".BID.".xml", true);
     
                                    $xpath2 = new domXPath($xml_out);
                                    $turn = $xpath2->query("//turn[last()]");
                                    
                                    if ($turn->length>0) {
                                        $xml_out_node=$turn->item(0);
                                        $timestamp=$xml_out_node->getAttribute("timestamp");
                                        
                                        if ($xml_out_node->getAttribute("name")==$curturn) {
                                            $actions = $xpath2->query("action", $xml_out_node);
                                            
                                            if ($actions->length>0) {
                                            
                                                foreach ($actions as $action_tags) {
                                                
                                                    if ($action_tags->getAttribute("mid") == $mid) {
                                                        $turned=1;
                                                    }
                                                }
    
                                            }
                                            // buggy
                                        }
    
                                    }
                                }
    
    //check player turns
    
    
                                $smarty->assign("turned",$turned);
                                
                                if ($turned==0) {
    //get list of enemy
                                    $emid=get_enemy_list($pilot_name);
                                    if ($emid>0) {
                                        $smarty->assign('emid', $emid );
                                        
                                        $status = $mech_tags->getElementsByTagName("status");
                                        $mech_status = "";
                                        
                                        if ($status->length > 0) {
                                            $mech_status = $status->item(0)->getAttribute("val");
                                        }
        
                                        if (empty($mech_status)) {
                                            $mech_status = "live";
                                        }
                                        
                                        if ($mech_status != "dead") {
        //get list of available weapons
                                            unset($weap_ids);
                                            $weap_ids=array();
                                            unset($weap_names);
                                            $weap_names=array();
            
                                            $body = $mech_tags->getElementsByTagName("body");
                                            $body_tags = $body->item(0);
                                            
                                            //optimize
                                            $pppoint=array("lh"=>"left hand","rh"=>"right hand","ll"=>"left leg","rl"=>"right leg","h"=>"head","f"=>"front", "r"=>"rear");
                                            while(list($vv, $kk)=each($pppoint)) {
            
                                                $$vv = $body_tags->getElementsByTagName($vv);
                                                
                                                //check if weapon HP>0
                                                //left hand
                                                ${$vv."_tags"} = $$vv->item(0);
                                                ${$vv."_weapon"} = ${$vv."_tags"}->getElementsByTagName("weapon");
                
                                                foreach (${$vv."_weapon"} as $weap_tags) {
                
                                                    $wid=$weap_tags->getAttribute("wid");
                
                                                    $weapon_name = $weap_tags->getElementsByTagName("name");
                                                    $wname=$weapon_name->item(0)->getAttribute("val");
                
                                                    $weapon_type = $weap_tags->getElementsByTagName("wtype");
                                                    $wtype=$weapon_type->item(0)->getAttribute("val");
                                                    
                                                    if (($wtype == "rocket") || ($wtype == "gun")) {
                                                        //get load amount
                                                        $load = $weap_tags->getElementsByTagName("load");
                                                        $la=0;
                                                        foreach ($load as $load_tags) {
                                                            $lid=$load_tags->getAttribute("lid");
                                                            
                                                            $z=$load_tags->getElementsByTagName("amount");
                                                            $load_amount=$z->item(0)->getAttribute("val");
                                                            $la+=$load_amount;
                                                        }
                                                    } else {
                                                        $la=1;
                                                    }
                                                    
                                                    $weapon_hp = $weap_tags->getElementsByTagName("hp");
                                                    $whp=$weapon_hp->item(0)->getAttribute("val");
                
                                                    if (($whp > 0) && ($la > 0)) {
                                                        $weap_ids[]=$wid;
                                                        $weap_names[]=$kk." - ".$wname." (".$wtype.")";
                                                    }
                                                }
                                            }
                                            
                                            $smarty->assign('weap_ids', $weap_ids );
                                            $smarty->assign('weap_names', $weap_names);
                                        } else {
                                            battle_end();
                                            $smarty->assign("error","You mech died.");
                                            $smarty->assign("onload", $battleendcode);
                                        }
                                    } else {
                                        battle_end();
                                        $smarty->assign("error","Enemy dead. Battle ended.");
                                        $smarty->assign("onload", $battleendcode);
                                    }
                                } else {
                                    $smarty->assign("time",TIMEOUT - time()+$timestamp);
                                    $smarty->assign("error","Turn ended. Waiting enemy turn.");
                                }
                            }
    
                        }
                    }
                }
                show_profiling();
                
                $smarty->display('battle.tpl');
    
            break;
    
            case "results":
                //write battle results to the screen
    //            global $smarty;

                $battle = open_xml(XMLDATA."battle/battle_".BID.".xml", true);
    
    
            //get mech info from last turn
            //if not, get from <teams>
    
                $xpath = new domXPath($battle);
                $battle_q = $xpath->query("/battle");
                
                if ($battle_q->length>0) {
                    $battle_root=$battle_q->item(0);
                }
    
                $turns = $xpath->query("turns");
                
                unset($fturns);
                
                if ($turns->length>0) {
                    $curturn=$turns->item(0)->getAttribute("current");
                    $fturns = $xpath->query("turn", $turns->item(0));
                }
    
                $mech = $xpath->query("turns//turn[@name=".($curturn-1)."]//mech");
                
                if ($mech->length>0) {
                } else {
                    $mech = $xpath->query("teams//team//mech");
    //                print  "use default mech";
                }
    
                $mechs= new DOMDocument();
                $mechs2 = $mechs->createElement("mechs");
                $mechs2 = $mechs->appendChild($mechs2);
    
                foreach ($mech as $mech_tags) {
                    $mechs2->appendChild($mechs->importNode($mech_tags, true));
                }
    
                $xpath3 = new domXPath($mechs);
    
                //get lastturn
    //             $lastturns = $xpath->query("turn[last()]", $turns->item(0));
    //             $lastturn=$lastturns->item(0);
    
    //             $fturns = $xpath->query("turn[last()-1]", $turns->item(0));
    //             $fturn=$fturns->item(0);
    
    
                
    
                unset($results);
                $results=array();
                $i=0;
    
                if (!empty($fturns)) {
                
                    $names=GetMechNames($mechs2);    
                
                    foreach ($fturns as $fturn) {
                        //if (isset($fturn)) {
                            $nturn=$fturn->getAttribute("name");
                            $results[$i].="<turn>Turn ".$nturn."</turn>";
                            $action_tags = $xpath->query("action", $fturn);
        
                            foreach($action_tags as $action) {
                                //get mech name
                                $mid=$action->getAttribute("mid");
                                
                                $m_name=$names[$mid];
        
                                //get movements
                                $move_tags = $xpath->query("move", $action);
                                $direction=$move_tags->item(0)->getAttribute("direction");
                                
                                switch($direction) {
                                    case "stay":
                                        $results[$i].="<mech mid=\"".$mid."\">".$m_name."</mech> <move>stay</move>.";
                                    break;
        
                                    case "left":
                                        $results[$i].="<mech mid=\"".$mid."\">".$m_name."</mech> <move>moved left</move>.";
                                    break;
        
                                    case "right":
                                        $results[$i].="<mech mid=\"".$mid."\">".$m_name."</mech> <move>moved rigth</move>.";
                                    break;
        
                                    case "forward":
                                        $results[$i].="<mech mid=\"".$mid."\">".$m_name."</mech> <move>walk forward</move>.";
                                    break;
        
                                    case "backward":
                                        $results[$i].="<mech mid=\"".$mid."\">".$m_name."</mech> <move>walk backward</move>.";
                                    break;
        
        
                                }
        
                                $fire_tags = $xpath->query("fire", $action);
                                foreach($fire_tags as $fire) {
                                    $emid=$fire->getAttribute("mid");
                                    $em_name=$names[$emid];
        
                                    $wid=$fire->getAttribute("wid");
                                    $amount=$fire->getAttribute("amount");
                                    $direction=$fire->getAttribute("direction");
        
        
                                    $results[$i].=" <mech mid=\"".$mid."\">".$m_name."</mech>  <action>fired</action> ".$direction." ";
                                    switch ($amount) {
                                        case 1:
                                            $results[$i].="once";
                                        break;
        
                                        case 2:
                                            $results[$i].="twice";
                                        break;
        
                                        default:
                                            $results[$i].=$amount;
                                        break;
        
                                    }
                                    $results[$i].=" to <enemy>".$em_name."</enemy>";
        
                                    $result_tags = $xpath->query("result", $fire);
                                    $fire_result=$result_tags->item(0);
            //                        check $fire_result
                                    if (isset($fire_result)) {
                                        $damage_tags = $xpath->query("damage", $fire_result);
        
                                    //
        
                                    // ///////
        
                                        $m=0;
                                        if ($damage_tags->length>0) {
                                        // print "ss";
        
                                            unset ($d_res);
                                            $pppoint=array("lh","rh","ll","rl","h","f", "r");
                                            while(list($kk, $vv)=each($pppoint)) {
        
                                                $pp_tags = $xpath->query($vv, $damage_tags->item(0));
                                                if ($pp_tags->length>0) {
        
                                                    $darmor_tags = $xpath->query("darmor", $pp_tags->item(0));
                                                    $m+=$darmor_tags->length;
        
                                                    foreach($darmor_tags as $darmor) {
                                                        unset($x);
                                                        $d_res.="armor ";
                                                        $x=$darmor->getAttribute("amount");
                                                        $d_res.="[".$x."] (".$vv.")";
                                                    }
        
                                                    $dweapon_tags = $xpath->query("dweapon", $pp_tags->item(0));
                                                    $m+=$dweapon_tags->length;
        
                                                    foreach($dweapon_tags as $dweapon) {
                                                        unset($x);
                                                        $d_res.="weapon ";
                                                        $x=$dweapon->getAttribute("amount");
                                                        $d_res.="[".$x."] (".$vv.")";
                                                    }
        
                                                    $ddevice_tags = $xpath->query("ddevice", $pp_tags->item(0));
                                                    $m+=$ddevice_tags->length;
        
                                                    foreach($ddevice_tags as $ddevice) {
                                                        unset($x);
                                                        $d_res.="device ";
                                                        $x=$ddevice->getAttribute("amount");
                                                        $d_res.="[".$x."] (".$vv.")";
                                                    }
                            
                                                    $dbody_tags = $xpath->query("dbody", $pp_tags->item(0));
                                                    $m+=$dbody_tags->length;
        
                                                    foreach($dbody_tags as $dbody) {
                                                        unset($x);
                                                        $d_res.="body ";
                                                        $x=$dbody->getAttribute("amount");
                                                        $d_res.="[".$x."] (".$vv.")";
                                                    }
        
                                                }
                                            }
                                        }
                                        //print " mm ";
                                        if ($m==0) {
                                            $results[$i].=" but misses.";
                                        } else {
                                            //show damage
                                            $results[$i].=" and hit ".$d_res;
        
                                        }
                                    }
                                }
                                $i++;
                        // }
                        }
                    }
                }
                if (!empty($results)) {
                    $results="<?xml version=\"1.0\" encoding=\"utf8\"?>\n<res>".$results."</res>";
                    //convert $results with xsl
                    //implode("<br/>", array_reverse($results))
                    $smarty->assign('results', $results);
                }
                
                
                show_profiling();
                $smarty->assign('end', $_GET["end"]);
                $smarty->display('results.tpl');
                
            break;
    
        }
    }

}

 if ($_GET["refresh"]==1) {
    show_profiling();
    $smarty->display('index.tpl');

}


//get list of enemy
function get_enemy_list($pilot_name) {
    global $smarty;

    $emid=0;
    unset($mech_ids);
    $mech_ids=array();
    unset($mech_name);
    $mech_name=array();

    $battle = open_xml(XMLDATA."battle/battle_".BID.".xml", true);

    $xpath = new domXPath($battle);

    $battle_q = $xpath->query("/battle");
    if ($battle_q->length>0) {
        $battle_root=$battle_q->item(0);
    }

    if (!empty($pilot_name)) {

//get mech info from last turn
//if not, get from <teams>

        $turns = $xpath->query("turns");
        if ($turns->length>0) {
            $curturn=$turns->item(0)->getAttribute("current");
        }

        $mech = $xpath->query("turns//turn[last()]//mech");

        if ($mech->length==0) {

            $mech = $xpath->query("teams//team//mech");

        }
    //current turn

        foreach ($mech as $mech_tags) {

            $pilot = $mech_tags->getElementsByTagName("pilot");
            $pilot_tags = $pilot->item(0);


            if ($pilot_tags->getAttribute("name") != $pilot_name) {
    //print   $pilot_tags->getAttribute("name");
            $status_tag = $mech_tags->getElementsByTagName("status");
            
                if ($status_tag->length > 0) {
                    $status=$status_tag->item(0)->getAttribute("val");
                    if ($status == "live") {
                        $emid=$mech_tags->getAttribute("mid");
                        $mech_ids[]=$mech_tags->getAttribute("mid");
                        $mech_name[]=$pilot_tags->getAttribute("name");
                    }
                }
            }
        }
    }
    //             $smarty->assign("mech_ids",$mech_ids);
       //                 $smarty->assign("mech_name",$mech_name);
return $emid;
}

//save xml file

function mech_info($isenemy=0) {
// info about mech in frame

    global $smarty;
    global $pilot_name;

            $battle = open_xml(XMLDATA."battle/battle_".BID.".xml", true);
            $xpath = new domXPath($battle);
            $xpath2 = new domXPath($battle);

            $battle_q = $xpath->query("/battle");
            if ($battle_q->length>0) {
                $battle_root=$battle_q->item(0);
            }

            if (!empty($pilot_name)) {
//get mech info from last turn
//if not, get from <teams>
                $turns = $xpath->query("turns");
                if ($turns->length>0) {
                    $curturn=$turns->item(0)->getAttribute("current");
                }

                $mech = $xpath->query("turns//turn[last()]//mech");
                if ($mech->length==0) {
                    $mech = $xpath->query("teams//team//mech");
                }
                $mech_init = $xpath2->query("teams//team//mech");
            
//------------
//rewrite for xpath
// // mech[pilot/@name='You']
                foreach ($mech as $mech_tags) {
                    $pilot = $mech_tags->getElementsByTagName("pilot");
                    $pilot_tags = $pilot->item(0);
                    $my_name = $pilot_tags->getAttribute("name");
                    if ((($my_name == $pilot_name) && ($isenemy == 0)) || (($my_name != $pilot_name) && ($isenemy == 1))) {
                        $my_a_name = $my_name;
                        $param = $mech_tags->getElementsByTagName("params");
                        $param_tags = $param->item(0);
            //rewrite to function
//init params
                        $Xmech_heat=0;
                        $Xmech_maxheat=0;
                        $Xmech_speed=0;
                        $Xmech_weight=0;
                        $Xmech_loadweight=0;
                        $Xmech_maxloadweight=0;

                        $param_status = $mech_tags->getElementsByTagName("status");
                        
                        if ($param_status->length > 0) {
                            $Xmech_status=$param_status->item(0)->getAttribute("val");
                        } else {
                            $Xmech_status="live";
                        }
                        
                        $smarty->assign("mech_status",$Xmech_status);

                        $param_heat = $param_tags->getElementsByTagName("heat");
                        $Xmech_heat=$param_heat->item(0)->getAttribute("val");
                        $smarty->assign("mech_heat",$Xmech_heat);

                        $param_maxheat = $param_tags->getElementsByTagName("maxheat");
                        $Xmech_maxheat=$param_maxheat->item(0)->getAttribute("val");
                        $smarty->assign("mech_maxheat",$Xmech_maxheat);

                        $param_speed = $param_tags->getElementsByTagName("speed");
                        $Xmech_speed=$param_speed->item(0)->getAttribute("val");
                        $smarty->assign("mech_speed",$Xmech_speed);

                        $param_weight = $param_tags->getElementsByTagName("weight");
                        if ($param_weight->length>0) {
                            $Xmech_weight=$param_weight->item(0)->getAttribute("val");
                            $smarty->assign("mech_weight",$Xmech_weight);
                        }

                        $param_loadweight = $param_tags->getElementsByTagName("loadweight");
                        if ($param_loadweight->length>0) {
                            $Xmech_loadweight=$param_loadweight->item(0)->getAttribute("val");
                            $smarty->assign("mech_loadweight",$Xmech_loadweight);
                        }

                        //$param_maxloadweight = $param_tags->getElementsByTagName("maxloadweight");
                        //if (count($param_maxloadweight)>0) {
                        // $Xmech_maxloadweight=$param_maxloadweight->item(0)->getAttribute("val");
                        $Xmech_maxloadweight=floor(0.5*$Xmech_weight);
                        $smarty->assign("mech_maxloadweight",$Xmech_maxloadweight);
                        //}


                        //get body info
                        unset($hp);
                        $hp=array();
                        $hp["h"]=0;
                        $hp["lh"]=0;
                        $hp["rh"]=0;
                        $hp["ll"]=0;
                        $hp["rl"]=0;
                        $hp["f"]=0;
                        $hp["r"]=0;

                    

                        $body = $mech_tags->getElementsByTagName("body");
                        $body_tags = $body->item(0);

                        //optimize 
                        $body_h = $body_tags->getElementsByTagName("h");
                        $body_h_tags = $body_h->item(0);

                        $h_hp=$body_h_tags->getElementsByTagName("hp");
                        if ($h_hp->length>0) {
                            $hp["h"]=$h_hp->item(0)->getAttribute("val");
                        }
                        
                        
                        $body_lh = $body_tags->getElementsByTagName("lh");
                        $body_lh_tags = $body_lh->item(0);

                        $lh_hp=$body_lh_tags->getElementsByTagName("hp");
                        if ($lh_hp->length>0) {
                            $hp["lh"]=$lh_hp->item(0)->getAttribute("val");
                        }


                        $body_rh = $body_tags->getElementsByTagName("rh");
                        $body_rh_tags = $body_rh->item(0);

                        $rh_hp=$body_rh_tags->getElementsByTagName("hp");
                        if ($rh_hp->length>0) {
                            $hp["rh"]=$rh_hp->item(0)->getAttribute("val");
                        }


                        $body_ll = $body_tags->getElementsByTagName("ll");
                        $body_ll_tags = $body_ll->item(0);

                        $ll_hp=$body_ll_tags->getElementsByTagName("hp");
                        if ($ll_hp->length>0) {
                            $hp["ll"]=$ll_hp->item(0)->getAttribute("val");
                        }

                        $body_rl = $body_tags->getElementsByTagName("rl");
                        $body_rl_tags = $body_rl->item(0);

                        $rl_hp=$body_rl_tags->getElementsByTagName("hp");
                        if ($rl_hp->length>0) {
                            $hp["rl"]=$rl_hp->item(0)->getAttribute("val");
                        }


                        $body_f = $body_tags->getElementsByTagName("f");
                        $body_f_tags = $body_f->item(0);

                        $f_hp=$body_f_tags->getElementsByTagName("hp");
                        if ($f_hp->length>0) {
                            $hp["f"]=$f_hp->item(0)->getAttribute("val");
                        }


                        $body_r = $body_tags->getElementsByTagName("r");
                        $body_r_tags = $body_r->item(0);

                        $r_hp=$body_r_tags->getElementsByTagName("hp");
                        if ($r_hp->length>0) {
                            $hp["r"]=$r_hp->item(0)->getAttribute("val");
                        }

//end of optimize
                        //------------

                        $pppoint=array("lh","rh","ll","rl","h","f", "r");
                        
                        while(list($k, $v)=each($pppoint)) {

                            unset($aid);
                            unset(${"a_".$v});
                            ${"a_".$v}=array();
                            
                            $ar=${"body_".$v."_tags"}->getElementsByTagName("armor");
                            
                            if ($ar->length>0) {
                                
                                foreach ($ar as $ar_tags) {
                                    unset($ainfo);
                                    $ainfo=array();
                                    $aid=$ar_tags->getAttribute("aid");
                                    
                                    unset($tmp);
                                    $tmp=$ar_tags->getElementsByTagName("name");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $ainfo["name"]=$tmp2;
                                    }
        
                                    unset($tmp);
                                    $tmp=$ar_tags->getElementsByTagName("hp");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $ainfo["hp"]=$tmp2;
                                    }
        
                                    unset($tmp);
                                    $tmp=$ar_tags->getElementsByTagName("lvl");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $ainfo["lvl"]=$tmp2;
                                    }
                                    unset($tmp);
                                    $tmp=$ar_tags->getElementsByTagName("weight");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $ainfo["weight"]=$tmp2;
                                    }
                                    unset($tmp);
                                    $tmp=$ar_tags->getElementsByTagName("dmgresist");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $ainfo["dmg"]=$tmp2;
                                    }
        
        
                                    ${"a_".$v}[$aid]=$ainfo;
                                }
                            }
                            
                            //  devices -----
                        $dv=${"body_".$v."_tags"}->getElementsByTagName("device");
                            
                            if ($dv->length>0) {
                                
                                foreach ($dv as $dv_tags) {
                                    unset($dinfo);
                                    $dinfo=array();
                                    $did=$dv_tags->getAttribute("did");
                                    
                                    unset($tmp);
                                    $tmp=$dv_tags->getElementsByTagName("name");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $dinfo["name"]=$tmp2;
                                    }
        
                                    unset($tmp);
                                    $tmp=$dv_tags->getElementsByTagName("hp");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $dinfo["hp"]=$tmp2;
                                    }
        
                                    unset($tmp);
                                    $tmp=$dv_tags->getElementsByTagName("lvl");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $dinfo["lvl"]=$tmp2;
                                    }
                                    unset($tmp);
                                    $tmp=$dv_tags->getElementsByTagName("weight");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $dinfo["weight"]=$tmp2;
                                    }

                                    //for cooler
                                    unset($tmp);
                                    $tmp=$dv_tags->getElementsByTagName("heating");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $dinfo["heating"]=$tmp2;
                                    }
        
        
                                    ${"d_".$v}[$did]=$dinfo;
                                }
                            }
    //-----------
                            
                            $wp=${"body_".$v."_tags"}->getElementsByTagName("weapon");
                            
                            if ($wp->length>0) {
                            
                                foreach ($wp as $wp_tags) {
                                    unset($winfo);
                                    $winfo=array();
                                    $wid=$wp_tags->getAttribute("wid");
                                    
                                    unset($tmp);
                                    $tmp=$wp_tags->getElementsByTagName("name");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $winfo["name"]=$tmp2;
                                    }
        
                                    unset($tmp);
                                    $tmp=$wp_tags->getElementsByTagName("hp");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $winfo["hp"]=$tmp2;
                                    }
        
                                    unset($tmp);
                                    $tmp=$wp_tags->getElementsByTagName("lvl");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $winfo["lvl"]=$tmp2;
                                    }
                                    
                                    unset($tmp);
                                    $tmp=$wp_tags->getElementsByTagName("weight");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $winfo["weight"]=$tmp2;
                                    }
                                    
                                    unset($tmp);
                                    $tmp=$wp_tags->getElementsByTagName("wtype");
                                    
                                    unset($wtype);
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $winfo["type"]=$tmp2;
                                        $wtype=$tmp2;
                                    }
                                    
                                    if ($wtype == "pulse") {
                                        unset($tmp);
                                        $tmp=$wp_tags->getElementsByTagName("distance");
                                        
                                        if ($tmp->length>0) {
                                            $tmp2=$tmp->item(0)->getAttribute("val");
                                        
                                            $winfo["distance"]=$tmp2;
                                        }
                                    
                                    }
                                    
                                    if (!$isenemy) {
                                        if (($wtype == "rocket")  || ($wtype == "gun")){
                                            $wpack=$wp_tags->getElementsByTagName("load");
                                        
                                            if ($wpack->length>0) {
                                            
                                                
                                                foreach ($wpack as $wpack_tags) {
                                                    unset($wpinfo);
                                                    unset($lid);
                                                    $lid=$wpack_tags->getAttribute("lid");
                                                        
                                                    unset($tmp);
                                                    $tmp=$wpack_tags->getElementsByTagName("name");
                                                    
                                                    if ($tmp->length>0) {
                                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                                        $wpinfo["name"]=$tmp2;
                                                    }
                        
                                                    unset($tmp);
                                                    $tmp=$wpack_tags->getElementsByTagName("amount");
                                                    
                                                    if ($tmp->length>0) {
                                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                                        $wpinfo["amount"]=$tmp2;
                                                    }
                        
                                                    unset($tmp);
                                                    $tmp=$wpack_tags->getElementsByTagName("lvl");
                                                    
                                                    if ($tmp->length>0) {
                                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                                        $wpinfo["lvl"]=$tmp2;
                                                    }
                                                    
                                                    unset($tmp);
                                                    $tmp=$wpack_tags->getElementsByTagName("weapdamage");
                                                    
                                                    if ($tmp->length>0) {
                                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                                        $wpinfo["dmg"]=$tmp2;
                                                    }
                                                    
                                                    unset($tmp);
                                                    $tmp=$wpack_tags->getElementsByTagName("loaduse");
                                                    
                                                    if ($tmp->length>0) {
                                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                                        $wpinfo["loaduse"]=$tmp2;
                                                    }
                                                            
                                                    unset($tmp);
                                                    $tmp=$wpack_tags->getElementsByTagName("distance");
                                                    
                                                    if ($tmp->length>0) {
                                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                                        $wpinfo["distance"]=$tmp2;
                                                    }
                                                    
                                                    unset($tmp);
                                                    $tmp=$wpack_tags->getElementsByTagName("weight");
                                                    
                                                    if ($tmp->length>0) {
                                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                                        $wpinfo["weight"]=$tmp2;
                                                    }          
                                                
                                                $winfo["pack"][$lid]=$wpinfo;
                                                }                                 
                                            }
                                        }
                                    }
                                
                                    ${"w_".$v}[$wid]=$winfo;
                                }
                            }
//----------
                        }
                    }
                }
//-----------
                    
                foreach ($mech_init as $mech_tags) {
                    $pilot = $mech_tags->getElementsByTagName("pilot");
                    $pilot_tags = $pilot->item(0);

                    $my_name = $pilot_tags->getAttribute("name");
                    if ((($my_name == $pilot_name) && ($isenemy == 0)) || (($my_name != $pilot_name) && ($isenemy == 1))) {

                        unset($hpmax);
                        $hpmax=array();
                        $hpmax["h"]=0;
                        $hpmax["lh"]=0;
                        $hpmax["rh"]=0;
                        $hpmax["ll"]=0;
                        $hpmax["rl"]=0;
                        $hpmax["f"]=0;
                        $hpmax["r"]=0;

                        $body = $mech_tags->getElementsByTagName("body");
                        $body_tags = $body->item(0);

                        $body_h = $body_tags->getElementsByTagName("h");
                        $body_h_tags = $body_h->item(0);

                        $h_hp=$body_h_tags->getElementsByTagName("hp");
                        if ($h_hp->length>0) {
                            $hpmax["h"]=$h_hp->item(0)->getAttribute("val");
                        }


                        $body_lh = $body_tags->getElementsByTagName("lh");
                        $body_lh_tags = $body_lh->item(0);

                        $lh_hp=$body_lh_tags->getElementsByTagName("hp");
                        if ($lh_hp->length>0) {
                            $hpmax["lh"]=$lh_hp->item(0)->getAttribute("val");
                        }


                        $body_rh = $body_tags->getElementsByTagName("rh");
                        $body_rh_tags = $body_rh->item(0);

                        $rh_hp=$body_rh_tags->getElementsByTagName("hp");
                        if ($rh_hp->length>0) {
                            $hpmax["rh"]=$rh_hp->item(0)->getAttribute("val");
                        }


                        $body_ll = $body_tags->getElementsByTagName("ll");
                        $body_ll_tags = $body_ll->item(0);

                        $ll_hp=$body_ll_tags->getElementsByTagName("hp");
                        if ($ll_hp->length>0) {
                            $hpmax["ll"]=$ll_hp->item(0)->getAttribute("val");
                        }

                        $body_rl = $body_tags->getElementsByTagName("rl");
                        $body_rl_tags = $body_rl->item(0);

                        $rl_hp=$body_rl_tags->getElementsByTagName("hp");
                        if ($rl_hp->length>0) {
                            $hpmax["rl"]=$rl_hp->item(0)->getAttribute("val");
                        }


                        $body_f = $body_tags->getElementsByTagName("f");
                        $body_f_tags = $body_f->item(0);

                        $f_hp=$body_f_tags->getElementsByTagName("hp");
                        if ($f_hp->length>0) {
                            $hpmax["f"]=$f_hp->item(0)->getAttribute("val");
                        }


                        $body_r = $body_tags->getElementsByTagName("r");
                        $body_r_tags = $body_r->item(0);

                        $r_hp=$body_r_tags->getElementsByTagName("hp");
                        if ($r_hp->length>0) {
                            $hpmax["r"]=$r_hp->item(0)->getAttribute("val");
                        }

//------------

                        $pppoint=array("lh","rh","ll","rl","h","f", "r");
                        
                        while(list($k, $v)=each($pppoint)) {

                            unset($aid);
                            
                            $ar=${"body_".$v."_tags"}->getElementsByTagName("armor");
                            
                            if ($ar->length>0) {
                                foreach ($ar as $ar_tags) {
                                    $aid=$ar_tags->getAttribute("aid");
                                    
        
                                    unset($tmp);
                                    $tmp=$ar_tags->getElementsByTagName("hp");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        ${"a_".$v}[$aid]["hpmax"]=$tmp2;
                                    }
                                }
                            }

                            
                            unset($did);
                            
                            $dv=${"body_".$v."_tags"}->getElementsByTagName("device");
                            
                            if ($dv->length>0) {
                                foreach ($dv as $dv_tags) {
                                    $did=$dv_tags->getAttribute("did");
                                    
        
                                    unset($tmp);
                                    $tmp=$dv_tags->getElementsByTagName("hp");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        ${"d_".$v}[$did]["hpmax"]=$tmp2;
                                    }
                                }
                            }
                            
                            $wp=${"body_".$v."_tags"}->getElementsByTagName("weapon");
                            
                            if ($wp->length>0) {
                                foreach ($wp as $wp_tags) {
                                    $wid=$wp_tags->getAttribute("wid");
                                    
        
                                    unset($tmp);
                                    $tmp=$wp_tags->getElementsByTagName("hp");
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        ${"w_".$v}[$wid]["hpmax"]=$tmp2;
                                    }
                                    
                                    unset($tmp);
                                    $tmp=$wp_tags->getElementsByTagName("wtype");
                                    
                                    unset($wtype);
                                    
                                    if ($tmp->length>0) {
                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                        $wtype=$tmp2;
                                    }
                                    
                                    if (!$isenemy) {
                                        if (($wtype == "rocket") || ($wtype == "gun")){
                                            $wpack=$wp_tags->getElementsByTagName("load");
                                        
                                            if ($wpack->length>0) {
                                            
                                                
                                                foreach ($wpack as $wpack_tags) {
                                                    unset($wpinfo);
                                                    unset($lid);
                                                    $lid=$wpack_tags->getAttribute("lid");
                                                        
                                                    unset($tmp);
                                                    $tmp=$wpack_tags->getElementsByTagName("amount");
                                                    
                                                    if ($tmp->length>0) {
                                                        $tmp2=$tmp->item(0)->getAttribute("val");
                                                        //$wpinfo["name"]=$tmp2;
                                                        ${"w_".$v}[$wid]["pack"][$lid]["amountmax"]=$tmp2;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            
                        }
//----------
                    }
                }
                
                $smarty->assign("hp",$hp);
                $smarty->assign("hpmax",$hpmax);

                //get armor info

                $smarty->assign("armor_h",$a_h);
                $smarty->assign("armor_lh",$a_lh);
                $smarty->assign("armor_rh",$a_rh);
                $smarty->assign("armor_f",$a_f);
                $smarty->assign("armor_r",$a_r);
                $smarty->assign("armor_ll",$a_ll);
                $smarty->assign("armor_rl",$a_rl);

                $smarty->assign("weapon_h",$w_h);
                $smarty->assign("weapon_lh",$w_lh);
                $smarty->assign("weapon_rh",$w_rh);
                $smarty->assign("weapon_f",$w_f);
                $smarty->assign("weapon_r",$w_r);
                $smarty->assign("weapon_ll",$w_ll);
                $smarty->assign("weapon_rl",$w_rl);
  
                $smarty->assign("device_h",$d_h);
                $smarty->assign("device_lh",$d_lh);
                $smarty->assign("device_rh",$d_rh);
                $smarty->assign("device_f",$d_f);
                $smarty->assign("device_r",$d_r);
                $smarty->assign("device_ll",$d_ll);
                $smarty->assign("device_rl",$d_rl);


                //get weapon info
 
                //get mech status
                $smarty->assign("mech_pilot_name",$my_a_name);
                $smarty->assign("isenemy",$isenemy);
    
                show_profiling();
                if ($isenemy==0) {
                    $smarty->display('uinfo.tpl');
                } else {
                    $smarty->display('einfo.tpl');
                }
            
            }
}

//get mech names
function GetMechNames($mechs) {
    
    global $smarty;
    
    unset($ret);
    $ret=array();
    $mech = $mechs->getElementsByTagName("mech");
    
    foreach ($mech as $mech_tags) {
        unset($mid);
        unset($name);
        $mid = $mech_tags->getAttribute("mid");
        $pilot = $mech_tags->getElementsByTagName("pilot");
        $pilot_tags = $pilot->item(0);
        $name = $pilot_tags->getAttribute("name");
        if ((!empty($mid)) && (!empty($name))) {
            $ret[$mid]=$name;
        }
    }
            
    return $ret;
}

function battle_end() {
//get list of mechs
//update status in battlelist
//update pilot status

    $battle = open_xml(XMLDATA."battle/battle_".BID.".xml", true);

    $xpath = new domXPath($battle);

    $battlelist = open_xml(XMLDATA."battle/battlelist.xml");

    $xpath2 = new domXPath($battlelist);

    $battle_l = $xpath2->query("//battle[@bid='".BID."']");

    if ($battle_l->length>0) {
                     
        $battle_q = $xpath->query("teams/team/mech/pilot");
        
        if ($battle_q->length>0) {
            
            foreach($battle_q as $pilot_tags) {
                
                $plogin=$pilot_tags->getAttribute("name");

//print $plogin;

                unset($xml_out);
                unset($xpath3);
                
                if (is_file(XMLDATA."pilot/pilot_".$plogin.".xml")) {
    
                    $xml_out = open_xml(XMLDATA."pilot/pilot_".$plogin.".xml");
                    $xpath3 = new domXPath($xml_out);

                    $battle_b = $xpath3->query("status/battle");
                    if ($battle_b->length>0) {
                        //$battlestatus=$battle->item(0)->getAttribute("status");
                        $battle_b->item(0)->setAttribute("status", "free");
                        $battle_b->item(0)->setAttribute("date", time());
                        save_xml(XMLDATA."pilot/pilot_".$plogin.".xml", $xml_out);
                    }
                    unlock_xml(XMLDATA."pilot/pilot_".$plogin.".xml");
                }
                
            }
            

            $parent=$battle_l->item(0);
            $parent->parentNode->removeChild($parent);
        }

        save_xml(XMLDATA."battle/battlelist.xml", $battlelist);
    }

    //unset($_SESSION["bid"]);
}
?>