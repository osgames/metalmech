<?php

//  !!!! BETA !!!!
//  install.php [15.12.2005], created by Mikhail Smakhtin [s600@users.sf.net]

if (file_exists('global.inc')) { 

    include_once 'global.inc';
    
    if (CONFIGURED=="yes") {

	echo "Configured (for new installation delete global.inc) \n";
	exit;
    }
}

if (file_exists('includes/constants.inc')) include_once 'includes/constants.inc';

?>
<table align="center" border="0" bgcolor="#000099" width="600" height="250">
<tr align="center" height="80"><td>
<h1><font color="#ffffff">METALMECH <? echo VER; ?> </font></h1>
</td></tr>
<?

function write_file($file_name,$file_data)
{
    $open_file = fopen($file_name,"w");
    fwrite($open_file,$file_data);
    fclose($open_file);
}

function check_install()
{
    if (!$_POST['check_post']) {

        print_install();
        exit;
    }

    echo '<tr><td><font color="#ffffff"><pre>';

    $html_end = "</pre></font></td></tr></table> \n";

    if (!file_exists('Smarty/Smarty.class.php')) {

        echo "SMARTY not found \n";
        $check_exit = 1;
    }

    if (!function_exists('domxml_version')) {

        echo "DOMXML not found \n";
        $check_exit = 1;
    }

    if (!function_exists('domxml_xslt_version')) {

        echo "DOMXML_XSLT not found \n";
        $check_exit = 1;
    }

    if ($check_exit) {

        echo $html_end;
        exit;
    }

    include_once 'Smarty/Smarty.class.php';

    $smarty = new Smarty;

    $check_php			= version_compare(phpversion(),		 "5.0.5",">=");
    //$check_domxml		= version_compare(domxml_version(),	 "xxxxx",">=");
    $check_smarty		= version_compare($smarty->_version,	 "2.6.9",">=");
    //$check_domxml_xslt	= version_compare(domxml_xslt_version(), "xxxxx",">=");

    if ($check_php		== 1) echo "PHP 	version ",phpversion(),		" - OK \n";
    //if ($check_domxml		== 1) echo "DOMXML 	version ",domxml_version(),	" - OK \n";
    if ($check_smarty		== 1) echo "SMARTY 	version ",$smarty->_version,	" - OK \n";
    //if ($check_domxml_xslt	== 1) echo "DOMXML_XSLT version ",domxml_xslt_version()," - OK \n";

    if ($check_php		== 0) echo "PHP 	version ",phpversion(),		", need => 5.0.5 \n";
    //if ($check_domxml		== 0) echo "DOMXML 	version ",domxml_version(),	", need => xxxxx \n";
    if ($check_smarty		== 0) echo "SMARTY 	version ",$smarty->_version,	", need => 2.6.9 \n";
    //if ($check_domxml_xslt	== 0) echo "DOMXML_XSLT version ",domxml_xslt_version(),", need => xxxxx \n";

    //if ( $check_php == 0 || $check_domxml == 0 || $check_smarty == 0 || $check_domxml_xslt == 0 ) {
    if ( $check_php == 0 || $check_smarty == 0 ) {

        echo $html_end;
        exit;
    }

    if ( file_exists($_POST['PATH']) && file_exists($_POST['DATA']) && file_exists($_POST['XMLDATA']) ) {

        if (fileperms($_POST['PATH']) < 16832) {

	    echo "Permission denied [",$_POST['PATH'],"] \n";
	    $perms_denied = 1;
	}

	if (fileperms($_POST['DATA']) < 16832) {

	    echo "Permission denied [",$_POST['DATA'],"] \n";
	    $perms_denied = 1;
	}

	if (fileperms($_POST['XMLDATA']) < 16832) {

	    echo "Permission denied [",$_POST['XMLDATA'],"] \n";
	    $perms_denied = 1;
	}

	if ($perms_denied) {

	    echo $html_end;
    	    exit;
	}

	$global_inc = '<?php
	define("CONFIGURED", "yes");
	define("PATH", "'.$_POST['PATH'].'");
        define("DATA", "'.$_POST['DATA'].'");
        define("XMLDATA", "'.$_POST['XMLDATA'].'");

        if (FALSE != DEBUG) {
        define("GLOBALSITE", "http://demo.mm");
        } else {
        define("GLOBALSITE", "'.$_POST['GLOBALSITE'].'");
        }

        $loginurl=GLOBALSITE."/pilot/?action=loginform";
        $globalsite=GLOBALSITE;    
        ?>';

        write_file("global.inc",$global_inc);

        chmod($_POST['XMLDATA'],0777);
        chown($_POST['XMLDATA'],$_ENV['USER']);

        echo "Completed the installation process \n";

    } else {

        if (!file_exists($_POST['PATH']))    echo "No directory found [",$_POST['PATH'],   "] \n";
        if (!file_exists($_POST['DATA']))    echo "No directory found [",$_POST['DATA'],   "] \n";
        if (!file_exists($_POST['XMLDATA'])) echo "No directory found [",$_POST['XMLDATA'],"] \n";
    }

    echo $html_end;
    exit;

}

function print_install()
{
    $doc_root = $_SERVER['DOCUMENT_ROOT'];
    define("PATH",$doc_root);
    define("DATA",$doc_root);
    define("XMLDATA","$doc_root/xml_data/");
    define("GLOBALSITE","http://".$_SERVER['HTTP_HOST']);

    ?>
    <tr><td>
    <form method="POST" action="install.php">
    <input style="border:1px solid white; background-color: #000099; color: #ffffff;" type="text" name="PATH"       value="<? echo PATH;       ?>" size="70"><font color="#ffffff"> - path        </font><br>
    <input style="border:1px solid white; background-color: #000099; color: #ffffff;" type="text" name="DATA"       value="<? echo DATA;       ?>" size="70"><font color="#ffffff"> - data        </font><br>
    <input style="border:1px solid white; background-color: #000099; color: #ffffff;" type="text" name="XMLDATA"    value="<? echo XMLDATA;    ?>" size="70"><font color="#ffffff"> - xml data    </font><br>
    <input style="border:1px solid white; background-color: #000099; color: #ffffff;" type="text" name="GLOBALSITE" value="<? echo GLOBALSITE; ?>" size="70"><font color="#ffffff"> - global site </font><br>
    <input type="hidden" name="check_post" value="1"><br>
    <center><input style="border:1px solid white; background-color: #ffffff;" type="submit" value="Install"></center>
    </form></td></tr></table>
    <?
}

check_install();
print_install();

?>


