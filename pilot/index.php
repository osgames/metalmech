<?php

/*
    This file is part of MetalMech.
    Copyright (C) 2005  Dzmitry A. Haiduchonak <boom@metalmech.com>

    MetalMech is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    MetalMech is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MetalMech; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

//pilot.php 


define("DEBUG", true);

if (FALSE != DEBUG) {

    require_once 'Benchmark/Timer.php';
 
    $timer = new Benchmark_Timer();
    $timer->start();
}

require 'Smarty/Smarty.class.php';

$smarty = new Smarty;

$smarty->compile_check = true;

if (FALSE != DEBUG) {
    $smarty->debugging = true;
} else {
    $smarty->debugging = false;
}

include_once("../global.inc");

if (CONFIGURED!="yes") {
print "Please use install.php";
die();
}

$smarty->template_dir = DATA.'/templates/pilot/';
$smarty->compile_dir = DATA.'/templates_c/pilot/';
$smarty->config_dir = DATA.'/configs/';

$site=GLOBALSITE."/pilot/";

require_once(DATA."/includes/functions.inc");

$smarty->assign("site", $site);
$smarty->assign("globalsite", $globalsite);

require_once(PATH."/includes/functions.inc");

session_start();

//get session id
$sid=session_id();
$cw=randomString(32);
$ip=get_ip();

if (empty($sid)) {
    print "No sid";
    //exception
    exit();
}

define("SID", $sid);
Warn("SID=".SID,__LINE__);
//get action
$action=$_GET["action"];
if (empty($action)) {
    $action=$_POST["action"];
}
    
switch($action) {

    case "newpilot":
    //new pilot registration
        $smarty->display('newpilot.tpl');
    break;
    
    case "newpilotreg":
    //new pilot registration
        $login=filter_login($_POST["login"]);
        
        $error="";

        //check if login already used
        
        if (empty($login)) {
            $error.="Type login<br/>";
        }

        
        if (is_file(XMLDATA."pilot/pilot_".$login.".xml")) {
            $error.="Login already used<br/>";
        }
        
        if (empty($_POST["nick"])) {
            $error.="Type nick<br/>";
        }
        
        if (empty($_POST["pwd"])) {
            $error.="Type password<br/>";
        }
        
        if (($_POST["pwd"] != $_POST["pwdc"]) || (empty($_POST["pwdc"]))){
            $error.="Confirm password<br/>";
        }

        if (empty($_POST["fname"])) {
            $error.="Type first name<br/>";
        }

        if (empty($_POST["lname"])) {
            $error.="Type last name<br/>";
        }
        
        if (empty($_POST["country"])) {
            $error.="Select country<br/>";
        }
        
         if (empty($_POST["email"])) {
            $error.="Type E-mail<br/>";
        } else {
            if (checkmail($_POST["email"]) != $_POST["email"]) {
                $error.="Wrong E-mail<br/>";
            }
        }
        
        
        if (md5(strtolower($_POST["confirm_word"])) != $_SESSION["confirm_word"]) {
            $error.="Confirm word not matched<br/>";
        }

        if (empty($error)) {
            $pilot = open_xml(PATH."/xml/pilot_default.xml", true);

            $xpath = new domXPath($pilot);
            
            $pilot_q = $xpath->query("/pilot");
            
            if ($pilot_q->length>0) {
                
                if (!is_file(XMLDATA."pilot/pilot_".$login.".xml")) {
                    
                    $pilot_root=$pilot_q->item(0);
                    $pilot_root->setAttribute("name",  co($_POST["nick"]));
                    $pilot_root->setAttribute("login", co($login));

                    $pilot_info = $xpath->query("/pilot/info");

                    if ($pilot_info->length>0) {
                        $pwd_tag = $pilot_info->item(0)->getElementsByTagName("password");
                        $pwd_tag->item(0)->setAttribute("val", co($_POST["pwd"]));

                        $name_tag = $pilot_info->item(0)->getElementsByTagName("name");
                        $name_tag->item(0)->setAttribute("first", co($_POST["fname"]));
                        $name_tag->item(0)->setAttribute("last", co($_POST["lname"]));

                        $country_tag = $pilot_info->item(0)->getElementsByTagName("country");
                        $country_tag->item(0)->setAttribute("val", co($_POST["country"]));
                    
                        $city_tag = $pilot_info->item(0)->getElementsByTagName("city");
                        $city_tag->item(0)->setAttribute("val", co($_POST["city"]));

                        $email_tag = $pilot_info->item(0)->getElementsByTagName("email");
                        $email_tag->item(0)->setAttribute("val", co($_POST["email"]));

                        $icq_tag = $pilot_info->item(0)->getElementsByTagName("icq");
                        $icq_tag->item(0)->setAttribute("val", co($_POST["icq"]));
                    }
                    save_xml(XMLDATA."pilot/pilot_".$login.".xml", $pilot, true);
                }
            }
            
            $smarty->assign("nick", $_POST["nick"]);
            $smarty->display('newpilotregok.tpl');
        } else {
            $smarty->assign("error", $error);
            $smarty->assign("login", $login);
            $smarty->assign("nick", $_POST["nick"]);
            $smarty->assign("fname", $_POST["fname"]);   
            $smarty->assign("lname", $_POST["lname"]);
            $smarty->assign("country", $_POST["country"]);
            $smarty->assign("city", $_POST["city"]);
            $smarty->assign("email", $_POST["email"]);
            $smarty->assign("icq", $_POST["icq"]);
            
            $smarty->display('newpilot.tpl');
        }
    break;
    
    case "login":
/*
1. open xml
2. check pwd
3. update loginstatus, sid, ip, cw
4. update session logined=1
*/

        $login=filter_login($_POST["login"]);

        if (empty($login)) {
            $error.="Type login<br/>";
        }
        
        if (empty($_POST["pwd"])) {
            $error.="Type password<br/>";
        }
        
        if (md5(strtolower($_POST["confirm_word"])) != $_SESSION["confirm_word"]) {
            $error.="Confirm word not matched<br/>";
        }
                
        if (empty($error)) {

            if (is_file(XMLDATA."pilot/pilot_".$login.".xml")) {
                $xml_out = open_xml(XMLDATA."pilot/pilot_".$login.".xml");

                $xpath = new domXPath($xml_out);

                $pwd = $xpath->query("info/password");
                $password=de($pwd->item(0)->getAttribute("val"));

                if ($password == $_POST["pwd"]) {

                    $pilot_login = $xpath->query("/pilot/login");

                    if ($pilot_login->length>0) {
                        $sid_tag = $pilot_login->item(0)->getElementsByTagName("sid");
                        $sid_tag->item(0)->setAttribute("val", co($sid));

                        $ip_tag = $pilot_login->item(0)->getElementsByTagName("ip");
                        $ip_tag->item(0)->setAttribute("val", co($ip));

                        $cw_tag = $pilot_login->item(0)->getElementsByTagName("cw");
                        $cw_tag->item(0)->setAttribute("val", co($cw));

                        $pilot_login->item(0)->setAttribute("status", "insystem");
                        $pilot_login->item(0)->setAttribute("date", time());

                        save_xml(XMLDATA."pilot/pilot_".$login.".xml", $xml_out);

                        $_SESSION["login"] = $login;
                        $_SESSION["verifycode"]=$cw;
                        $_SESSION["insystem"] = 1;
                        $_SESSION["ip"] = $ip;

                        if (empty($_POST["returl"])) {
                            $returl=$site;
                        } else {
                            $returl=$_POST["returl"];
                        }

                        header("Location: ".$returl);
                    }
                } else {
                    $error.="Mismatch password<br/>";
                }
            } else {
                $error.="Unknown pilot<br/>";
            }
        }

        if (!empty($error)) {
            $smarty->assign("login", $login);
            $smarty->assign("returl", $_POST["returl"]);
            $smarty->assign("error", $error);
            $smarty->display('login.tpl');
        }
    break;

    

    case "pilotinfo":
        if (chk_login()) {
            
            $login=filter_login($_SESSION["login"]);

            if (is_file(XMLDATA."pilot/pilot_".$login.".xml")) {
                $xml_out = open_xml(XMLDATA."pilot/pilot_".$login.".xml", true);


                $xpath = new domXPath($xml_out);

                $pilot = $xpath->query("/pilot");
                $login2=de($pilot->item(0)->getAttribute("login"));
                $nick=de($pilot->item(0)->getAttribute("name"));

                if ($login == $login2) {

                    $pilot_info = $xpath->query("/pilot/info");

                    if ($pilot_info->length>0) {
                        $pwd_tag = $pilot_info->item(0)->getElementsByTagName("password");
                        $pwd = de($pwd_tag->item(0)->getAttribute("val"));

                        $name_tag = $pilot_info->item(0)->getElementsByTagName("name");
                        $fname = de($name_tag->item(0)->getAttribute("first"));
                        $lname = de($name_tag->item(0)->getAttribute("last"));

                        $country_tag = $pilot_info->item(0)->getElementsByTagName("country");
                        $country = de($country_tag->item(0)->getAttribute("val"));

                        $city_tag = $pilot_info->item(0)->getElementsByTagName("city");
                        $city = de($city_tag->item(0)->getAttribute("val"));

                        $email_tag = $pilot_info->item(0)->getElementsByTagName("email");
                        $email = de($email_tag->item(0)->getAttribute("val"));

                        $icq_tag = $pilot_info->item(0)->getElementsByTagName("icq");
                        $icq = de($icq_tag->item(0)->getAttribute("val"));

                    }

                    $smarty->assign("login", $login2);
                    $smarty->assign("nick", $nick);
                    $smarty->assign("fname", $fname);
                    $smarty->assign("lname", $lname);
                    $smarty->assign("country", $country);
                    $smarty->assign("city", $city);
                    $smarty->assign("email", $email);
                    $smarty->assign("icq", $icq);


                    $smarty->display('editpilot.tpl');
                }
            }
        } else {
            go_to_login($_SERVER["REQUEST_URI"]);
        }
        
    break;

    case "pilotchange":

 //change pilot info
        if (chk_login()) {
            $error="";
            $login=filter_login($_SESSION["login"]);
            //check if login already used
            
    
            
            if (empty($_POST["nick"])) {
                $error.="Type nick<br/>";
            }
            
            
            if (!empty($_POST["pwd"]) && (($_POST["pwd"] != $_POST["pwdc"]) || (empty($_POST["pwdc"])))){
                $error.="Confirm password<br/>";
            }
    
            if (empty($_POST["fname"])) {
                $error.="Type first name<br/>";
            }
    
            if (empty($_POST["lname"])) {
                $error.="Type last name<br/>";
            }
            
            if (empty($_POST["country"])) {
                $error.="Select country<br/>";
            }
            
            if (empty($_POST["email"])) {
                $error.="Type E-mail<br/>";
            } else {
                if (checkmail($_POST["email"]) != $_POST["email"]) {
                    $error.="Wrong E-mail<br/>";
                }
            }
            
            if (md5(strtolower($_POST["confirm_word"])) != $_SESSION["confirm_word"]) {
                $error.="Confirm word not matched<br/>";
            }
    
            if (empty($error)) {
    
                    
                if (!is_file(XMLDATA."pilot/pilot_".$login.".xml")) {
                
                    $pilot = open_xml(XMLDATA."pilot/pilot_".$login.".xml");
                        
                    $xpath = new domXPath($pilot);
                    
                    $pilot_q = $xpath->query("/pilot");
                    
                    if ($pilot_q->length>0) {
                        
                        $pilot_root=$pilot_q->item(0);
                        $pilot_root->setAttribute("name", co($_POST["nick"]));
//                        $pilot_root->setAttribute("login", $_POST["login"]);
    
                        $pilot_info = $xpath->query("/pilot/info");
    
                        if ($pilot_info->length>0) {
                            
                            if (!empty($_POST["pwd"])) {
                                $pwd_tag = $pilot_info->item(0)->getElementsByTagName("password");
                                $pwd_tag->item(0)->setAttribute("val", co($_POST["pwd"]));
                            }
    
                            $name_tag = $pilot_info->item(0)->getElementsByTagName("name");
                            $name_tag->item(0)->setAttribute("first", co($_POST["fname"]));
                            $name_tag->item(0)->setAttribute("last", co($_POST["lname"]));
    
                            $country_tag = $pilot_info->item(0)->getElementsByTagName("country");
                            $country_tag->item(0)->setAttribute("val", co($_POST["country"]));
                        
                            $city_tag = $pilot_info->item(0)->getElementsByTagName("city");
                            $city_tag->item(0)->setAttribute("val", co($_POST["city"]));
    
                            $email_tag = $pilot_info->item(0)->getElementsByTagName("email");
                            $email_tag->item(0)->setAttribute("val", co($_POST["email"]));
    
                            $icq_tag = $pilot_info->item(0)->getElementsByTagName("icq");
                            $icq_tag->item(0)->setAttribute("val", co($_POST["icq"]));
                        }
                        
                        save_xml(XMLDATA."pilot/pilot_".$login.".xml", $pilot);
                    }
                }
                
                $smarty->assign("nick", $_POST["nick"]);
                $smarty->display('pilotchangeok.tpl');
            } else {
                $smarty->assign("error", $error);
                $smarty->assign("login", $login);
                $smarty->assign("nick", $_POST["nick"]);
                $smarty->assign("fname", $_POST["fname"]);   
                $smarty->assign("lname", $_POST["lname"]);
                $smarty->assign("country", $_POST["country"]);
                $smarty->assign("city", $_POST["city"]);
                $smarty->assign("email", $_POST["email"]);
                $smarty->assign("icq", $_POST["icq"]);
                
                $smarty->display('pilotinfo.tpl');
            }
        }
    break;

    case "loginform":
        if (empty($_GET["returl"])) {
            $returl=$site;
        } else {
            $returl=$_GET["returl"];
        }
        $smarty->assign("returl", $returl);
        $smarty->display('login.tpl');
    break;

    case "logout":
        if (chk_login()) {
            $_SESSION["login"] = "";
            $_SESSION["verifycode"]="";
            $_SESSION["insystem"] = 0;
            $_SESSION["ip"] = "";
            $smarty->display('logout.tpl');
        } else {
            go_to_login($_SERVER["REQUEST_URI"]);
        }
    break;

    case "remember":

        $smarty->display('remember.tpl');

    break;

    case "remembersubmit":

        $login=filter_login($_POST["login"]);

        if (empty($login)) {
            $error.="Type login<br/>";
        }
        
        if (md5(strtolower($_POST["confirm_word"])) != $_SESSION["confirm_word"]) {
            $error.="Confirm word not matched<br/>";
        }
                
        if (empty($error)) {

            if (is_file(XMLDATA."pilot/pilot_".$login.".xml")) {
                $xml_out = open_xml(XMLDATA."pilot/pilot_".$login.".xml");
                    
                $xpath = new domXPath($xml_out);
                $pilot_info = $xpath->query("/pilot/info");

                if ($pilot_info->length>0) {

                    $pwd = $xpath->query("info/password");
                    $newpwd = randomString(6);
                    $password=$pwd->item(0)->setAttribute("val", co($newpwd));

                    $email_tag = $pilot_info->item(0)->getElementsByTagName("email");
                    $email = de($email_tag->item(0)->getAttribute("val"));
            // send mail
                    save_xml(XMLDATA."pilot/pilot_".$login.".xml", $xml_out);
                    $msg .= "\n\nYour password changed to \"".$newpwd."\".\n\n\nboom@metalmech.com\n";
                    mail($email, "MetalMech New Password", $msg, "From: robot@metalmech.com\r\nReply-To: boom@metalmech.com");
                    $smarty->display('rememberok.tpl');
                } else {
                    $error.="Unknown error<br/>";
                }

            } else {
                $error.="Unknown pilot<br/>";
            }
        }

        if (!empty($error)) {
            $smarty->assign("login", $login);
            $smarty->assign("error", $error);
            $smarty->display('remember.tpl');
        }
    break;

    case "online":
    //list online users
    //DEBUG only
    //move to admin

        $users=get_online_users();
        //print_r($users);

         $smarty->assign("users", $users);
        $smarty->display('online.tpl');
    break;
    

    default:
        if (chk_login()) {
            $smarty->display('menu.tpl');
        } else {
            go_to_login($_SERVER["REQUEST_URI"]);
        }
    break;
    
   /* default:
        $smarty->display('index.tpl');
    break;
    */
    
}
?>